extern crate fastxml;

use std::fs;

// Edinburgh University tests

//errata2e
#[test]
#[ignore = "invalid"]
fn eduni_errata2e_e2a() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E2a.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "invalid"]
fn eduni_errata2e_e2b() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E2b.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn eduni_errata2e_e9a() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E9a.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "invalid"]
fn eduni_errata2e_e9b() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E9b.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn eduni_errata2e_e15e() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E15e.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e15f() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E15f.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e15i() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E15i.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e15j() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E15j.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e15k() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E15k.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e15l() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E15l.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "external entities"]
fn eduni_errata2e_e18() {
    let parsed = fastxml::parse_file("./tests/xmlconf/eduni/errata-2e/E18.xml").unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "external entities"]
fn eduni_errata2e_e19() {
    let parsed = fastxml::parse_file("./tests/xmlconf/eduni/errata-2e/E19.xml").unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e22() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E22.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e24() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E24.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
#[should_panic]
// invalid UTF-8
fn eduni_errata2e_e27() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E27.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn eduni_errata2e_e29() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E29.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e36() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E36.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e41() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E41.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e48() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E48.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "XML 1.1"]
fn eduni_errata2e_e50() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E50.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "external entities can contain conditional sections"]
fn eduni_errata2e_e60() {
    let parsed = fastxml::parse_file("./tests/xmlconf/eduni/errata-2e/E60.xml").unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn eduni_errata2e_e61() {
    let contents = fs::read("./tests/xmlconf/eduni/errata-2e/E61.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

//errata3e
//errata4e
//misc
//nse
//ns1.0
//ns1.1
//xml1.1
