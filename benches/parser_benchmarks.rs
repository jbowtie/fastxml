#[macro_use]
extern crate criterion;

use criterion::Criterion;

extern crate fastxml;
use std::fs;

fn parse_file(filename: &str) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    let contents = fs::read(filename)?;
    Ok(contents)
}

// approx 22us on local; target 15us
fn vtd_benchmark(c: &mut Criterion) {
    let vtd = parse_file("tests/vtd-example.xml").unwrap();
    c.bench_function("parse vtd-example", move |b| {
        b.iter(|| fastxml::parse_doc(&vtd))
    });
}

// approx 6.5ms on local; target 3ms
fn hamlet_benchmark(c: &mut Criterion) {
    let hamlet = parse_file("tests/hamlet.xml").unwrap();
    c.bench_function("parse 274k hamlet", move |b| {
        b.iter(|| fastxml::parse_doc(&hamlet))
    });
}

// approx 60ms on local; target 30ms
fn ot_benchmark(c: &mut Criterion) {
    let ot = parse_file("tests/ot.xml").unwrap();
    c.bench_function("parse 3.5MB ot.xml", move |b| {
        b.iter(|| fastxml::parse_doc(&ot))
    });
}

// currently unused; appropriate for benchmarks
// that are estimated more than 600s
fn _long_running_config() -> Criterion {
    let c = Criterion::default();
    c.sample_size(3)
}

fn std_config() -> Criterion {
    let c = Criterion::default();
    c.sample_size(20)
}

criterion_group!(name=benches; config=std_config(); targets=vtd_benchmark, hamlet_benchmark, ot_benchmark);
//criterion_group!(name=long; config=long_running_config(); targets=hamlet_benchmark);
//criterion_main!(benches, long);
criterion_main!(benches);
