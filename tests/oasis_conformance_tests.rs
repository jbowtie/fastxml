extern crate fastxml;

use std::fs;

// OASIS/NIST XML 1.0 Tests

// valid
#[test]
fn oasis_valid_p01pass2() {
    let contents = fs::read("./tests/xmlconf/oasis/p01pass2.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p06pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p06pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p07pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p07pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p08pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p08pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p09pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p09pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p12pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p12pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p22pass4() {
    let contents = fs::read("./tests/xmlconf/oasis/p22pass4.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p22pass5() {
    let contents = fs::read("./tests/xmlconf/oasis/p22pass5.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p22pass6() {
    let contents = fs::read("./tests/xmlconf/oasis/p22pass6.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p28pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p28pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p28pass3() {
    let contents = fs::read("./tests/xmlconf/oasis/p28pass3.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p28pass4() {
    let contents = fs::read("./tests/xmlconf/oasis/p28pass4.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p28pass5() {
    let contents = fs::read("./tests/xmlconf/oasis/p28pass5.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p29pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p29pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p30pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p30pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p30pass2() {
    let contents = fs::read("./tests/xmlconf/oasis/p30pass2.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p31pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p31pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p31pass2() {
    let contents = fs::read("./tests/xmlconf/oasis/p31pass2.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p43pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p43pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("elem", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p45pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p45pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p46pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p46pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p47pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p47pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p48pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p48pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p49pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p49pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p50pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p50pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p51pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p51pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p52pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p52pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p53pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p53pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p54pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p54pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p55pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p55pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p56pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p56pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p57pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p57pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p58pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p58pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p59pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p59pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p60pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p60pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p61pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p61pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p62pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p62pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p63pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p63pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p64pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p64pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p68pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p68pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p69pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p69pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p70pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p70pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p71pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p71pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p72pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p72pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p73pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p73pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn oasis_valid_p76pass1() {
    let contents = fs::read("./tests/xmlconf/oasis/p76pass1.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

// invalid
// not-wf

#[test]
fn oasis_not_wf_p01fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p01fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn oasis_not_wf_p01fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p01fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn oasis_not_wf_p01fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p01fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn oasis_not_wf_p01fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p01fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn oasis_not_wf_p02fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail7() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail7.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail8() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail8.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail9() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail9.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail10() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail11() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail12() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail13() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail14() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail15() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail15.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail16() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail16.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail17() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail17.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail18() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail18.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail19() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail19.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail20() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail20.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail21() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail21.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail22() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail22.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail23() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail23.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail24() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail24.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail25() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail25.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail26() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail26.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail27() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail27.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail28() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail28.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail29() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail29.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
#[ignore = "Investigate: #FFFE should be considered invalid"]
fn oasis_not_wf_p02fail30() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail30.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p02fail31() {
    let contents = fs::read("./tests/xmlconf/oasis/p02fail31.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail7() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail7.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail8() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail8.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail9() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail9.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail10() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail11() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail12() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail13() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail14() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail15() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail15.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail16() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail16.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail17() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail17.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail18() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail18.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail19() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail19.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail20() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail20.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail21() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail21.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail22() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail22.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail23() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail23.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail24() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail24.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail25() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail25.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail26() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail26.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail27() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail27.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail28() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail28.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p03fail29() {
    let contents = fs::read("./tests/xmlconf/oasis/p03fail29.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn oasis_not_wf_p04fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p04fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn oasis_not_wf_p04fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p04fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn oasis_not_wf_p04fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p04fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn oasis_not_wf_p05fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p05fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p05fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p05fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p05fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p05fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p05fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p05fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p05fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p05fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p09fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p09fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p09fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p09fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn oasis_not_wf_p09fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p09fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn oasis_not_wf_p09fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p09fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn oasis_not_wf_p09fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p09fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn oasis_not_wf_p10fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p10fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn oasis_not_wf_p10fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p10fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn oasis_not_wf_p10fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p10fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn oasis_not_wf_p11fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p11fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn oasis_not_wf_p11fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p11fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of notation definition)");
}

#[test]
fn oasis_not_wf_p12fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p12fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p12fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p12fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p12fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p12fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p12fail7() {
    let contents = fs::read("./tests/xmlconf/oasis/p12fail7.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn oasis_not_wf_p14fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p14fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p14fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p14fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn oasis_not_wf_p14fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p14fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn oasis_not_wf_p15fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p15fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Comment improperly terminated"));
}

#[test]
fn oasis_not_wf_p15fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p15fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Comment improperly terminated"));
}

#[test]
fn oasis_not_wf_p15fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p15fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Comment improperly terminated"));
}

#[test]
fn oasis_not_wf_p16fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p16fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn oasis_not_wf_p16fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p16fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid PI tag");
}

#[test]
fn oasis_not_wf_p16fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p16fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 1: missing required whitespace");
}

#[test]
fn oasis_not_wf_p18fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p18fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn oasis_not_wf_p18fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p18fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn oasis_not_wf_p18fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p18fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn oasis_not_wf_p22fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p22fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn oasis_not_wf_p22fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p22fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn oasis_not_wf_p23fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p23fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn oasis_not_wf_p23fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p23fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn oasis_not_wf_p23fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p23fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn oasis_not_wf_p23fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p23fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn oasis_not_wf_p23fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p23fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn oasis_not_wf_p24fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p24fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn oasis_not_wf_p24fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p24fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn oasis_not_wf_p25fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p25fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn oasis_not_wf_p26fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p26fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn oasis_not_wf_p26fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p26fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn oasis_not_wf_p27fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p27fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character");
}

#[test]
fn oasis_not_wf_p28fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p28fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn oasis_not_wf_p29fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p29fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p30fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p30fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
#[ignore = "Should fail because there is no root element"]
fn oasis_not_wf_p31fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p31fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn oasis_not_wf_p32fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p32fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration: invalid closing quote");
}

#[test]
fn oasis_not_wf_p32fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p32fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration: invalid closing quote");
}

#[test]
fn oasis_not_wf_p32fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p32fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn oasis_not_wf_p32fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p32fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected opening quote)"
    );
}

#[test]
fn oasis_not_wf_p32fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p32fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: when present, standalone must be \'yes\' or \'no\'"
    );
}

#[test]
fn oasis_not_wf_p39fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p39fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn oasis_not_wf_p39fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p39fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn oasis_not_wf_p39fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p39fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Not enough bytes");
}

#[test]
fn oasis_not_wf_p39fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p39fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn oasis_not_wf_p39fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p39fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn oasis_not_wf_p40fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p40fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
fn oasis_not_wf_p40fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p40fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p40fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p40fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p40fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p40fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p41fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p41fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn oasis_not_wf_p41fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p41fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn oasis_not_wf_p41fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p41fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid char in attribute");
}

#[test]
fn oasis_not_wf_p42fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p42fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn oasis_not_wf_p42fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p42fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag not properly formed");
}

#[test]
fn oasis_not_wf_p42fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p42fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn oasis_not_wf_p43fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p43fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn oasis_not_wf_p43fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p43fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn oasis_not_wf_p43fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p43fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn oasis_not_wf_p44fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p44fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn oasis_not_wf_p44fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p44fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn oasis_not_wf_p44fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p44fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn oasis_not_wf_p44fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p44fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
fn oasis_not_wf_p44fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p44fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "duplicate attribute");
}

#[test]
fn oasis_not_wf_p45fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p45fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn oasis_not_wf_p45fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p45fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn oasis_not_wf_p45fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p45fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn oasis_not_wf_p45fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p45fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p46fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p46fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn oasis_not_wf_p46fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p46fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p46fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p46fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p46fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p46fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p46fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p46fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p46fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p46fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Expected \'>\' to end element declaration");
}

#[test]
fn oasis_not_wf_p47fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p47fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn oasis_not_wf_p47fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p47fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p47fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p47fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn oasis_not_wf_p47fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p47fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p48fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p48fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn oasis_not_wf_p48fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p48fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn oasis_not_wf_p49fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p49fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn oasis_not_wf_p50fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p50fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn oasis_not_wf_p51fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p51fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: expected \'>\' at this location");
}

#[test]
fn oasis_not_wf_p51fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn oasis_not_wf_p51fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'*\' at this location");
}

#[test]
fn oasis_not_wf_p51fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn oasis_not_wf_p51fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn oasis_not_wf_p51fail7() {
    let contents = fs::read("./tests/xmlconf/oasis/p51fail7.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn oasis_not_wf_p52fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p52fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "ATTLIST name is required");
}

#[test]
fn oasis_not_wf_p52fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p52fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p53fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p53fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p53fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p53fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p53fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p53fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn oasis_not_wf_p53fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p53fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p53fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p53fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn oasis_not_wf_p54fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p54fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn oasis_not_wf_p55fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p55fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn oasis_not_wf_p56fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p56fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p56fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p56fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 4: expected \'NOTATION\' at this location"
    );
}

#[test]
fn oasis_not_wf_p56fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p56fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 4: expected \'NOTATION\' at this location"
    );
}

#[test]
fn oasis_not_wf_p56fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p56fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p56fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p56fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn oasis_not_wf_p57fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p57fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn oasis_not_wf_p58fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NAME in attribute definition");
}

#[test]
fn oasis_not_wf_p58fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 6: character \',\' not allowed in attribute definition"
    );
}

#[test]
fn oasis_not_wf_p58fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NAME in attribute definition");
}

#[test]
fn oasis_not_wf_p58fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn oasis_not_wf_p58fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn oasis_not_wf_p58fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: expected \'(\' at this location");
}

#[test]
fn oasis_not_wf_p58fail7() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail7.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: expected \'(\' at this location");
}

#[test]
fn oasis_not_wf_p58fail8() {
    let contents = fs::read("./tests/xmlconf/oasis/p58fail8.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NAME in attribute definition");
}

#[test]
fn oasis_not_wf_p59fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p59fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NMTOKEN in attribute definition");
}

#[test]
fn oasis_not_wf_p59fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p59fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 4: character \',\' not allowed in attribute definition"
    );
}

#[test]
fn oasis_not_wf_p59fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p59fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NMTOKEN in attribute definition");
}

#[test]
fn oasis_not_wf_p60fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p60fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn oasis_not_wf_p60fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p60fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p60fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p60fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn oasis_not_wf_p60fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p60fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p60fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p60fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p61fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p61fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p62fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p62fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p62fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p62fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p63fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p63fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p63fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p63fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p64fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p64fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p64fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p64fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn oasis_not_wf_p66fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p66fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn oasis_not_wf_p66fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p66fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn oasis_not_wf_p66fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p66fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn oasis_not_wf_p66fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p66fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p66fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p66fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn oasis_not_wf_p66fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p66fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn oasis_not_wf_p68fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p68fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 8: character \'\r\' not allowed  in entity name"
    );
}

#[test]
fn oasis_not_wf_p68fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p68fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn oasis_not_wf_p68fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p68fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 8: character \' \' not allowed  in entity name"
    );
}

#[test]
fn oasis_not_wf_p69fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p69fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 5: character \'<\' not allowed  in entity name"
    );
}

#[test]
fn oasis_not_wf_p69fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p69fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 5: character \' \' not allowed  in entity name"
    );
}

#[test]
fn oasis_not_wf_p69fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p69fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 5: character \' \' not allowed  in entity name"
    );
}

#[test]
fn oasis_not_wf_p70fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p70fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn oasis_not_wf_p71fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p71fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p71fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p71fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn oasis_not_wf_p71fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p71fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn oasis_not_wf_p71fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p71fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p72fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p72fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p72fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p72fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p72fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p72fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p72fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p72fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn oasis_not_wf_p73fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p73fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn oasis_not_wf_p73fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p73fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p73fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p73fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p73fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p73fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn oasis_not_wf_p73fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p73fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn oasis_not_wf_p74fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p74fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p74fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p74fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn oasis_not_wf_p74fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p74fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p75fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p75fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn oasis_not_wf_p75fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p75fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn oasis_not_wf_p75fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p75fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn oasis_not_wf_p75fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p75fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p75fail5() {
    let contents = fs::read("./tests/xmlconf/oasis/p75fail5.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn oasis_not_wf_p75fail6() {
    let contents = fs::read("./tests/xmlconf/oasis/p75fail6.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn oasis_not_wf_p76fail1() {
    let contents = fs::read("./tests/xmlconf/oasis/p76fail1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p76fail2() {
    let contents = fs::read("./tests/xmlconf/oasis/p76fail2.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn oasis_not_wf_p76fail3() {
    let contents = fs::read("./tests/xmlconf/oasis/p76fail3.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn oasis_not_wf_p76fail4() {
    let contents = fs::read("./tests/xmlconf/oasis/p76fail4.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn oasis_not_wf_p11pass1() {
    // system literals may not contain URI fragments
    let contents = fs::read("./tests/xmlconf/oasis/p11pass1.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 5: character \'#\' not allowed in SYSTEM identifier"
    );
}
