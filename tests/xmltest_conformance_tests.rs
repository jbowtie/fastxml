extern crate fastxml;

use std::fs;

// James Clark  XML 1.0 Tests

#[test]
fn jclark_not_wf_sa_001() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/001.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_002() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/002.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn jclark_not_wf_sa_003() {
    // Processing Instruction target name is required.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/003.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid PI tag");
}

#[test]
fn jclark_not_wf_sa_004() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/004.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_005() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/005.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_006() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/006.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Comment improperly terminated; two hyphens (\'--\') is a prohibited sequence in comments"
    );
}

#[test]
fn jclark_not_wf_sa_007() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/007.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 1: character \' \' not allowed  in entity name"
    );
}

#[test]
fn jclark_not_wf_sa_008() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/008.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_009() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/009.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn jclark_not_wf_sa_010() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/010.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_011() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/011.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid char in attribute");
}

#[test]
fn jclark_not_wf_sa_012() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/012.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Attribute value: unexpected character (expected opening quote)"
    );
}

#[test]
fn jclark_not_wf_sa_013() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/013.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn jclark_not_wf_sa_014() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/014.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn jclark_not_wf_sa_015() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/015.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Attribute value: unexpected character (expected opening quote)"
    );
}

#[test]
fn jclark_not_wf_sa_016() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/016.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_017() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/017.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_018() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/018.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn jclark_not_wf_sa_019() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/019.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn jclark_not_wf_sa_020() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/020.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_021() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/021.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 1: character \'\"\' not allowed  in entity name"
    );
}

#[test]
fn jclark_not_wf_sa_022() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/022.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn jclark_not_wf_sa_023() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/023.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_024() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/024.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn jclark_not_wf_sa_025() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/025.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn jclark_not_wf_sa_026() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/026.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn jclark_not_wf_sa_027() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/027.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_028() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/028.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_029() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/029.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn jclark_not_wf_sa_030() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/030.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_031() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/031.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_032() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/032.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_033() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/033.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_034() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/034.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_035() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/035.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn jclark_not_wf_sa_036() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/036.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_037() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/037.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_038() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/038.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "duplicate attribute");
}

#[test]
fn jclark_not_wf_sa_039() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/039.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn jclark_not_wf_sa_040() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/040.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_041() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/041.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_042() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/042.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_043() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/043.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_044() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/044.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_045() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/045.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_046() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/046.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_047() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/047.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_048() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/048.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_049() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/049.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn jclark_not_wf_sa_050() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/050.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Not enough bytes");
}

#[test]
fn jclark_not_wf_sa_051() {
    //CData invalid at top of document
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/051.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "CDATA not allowed here");
}

#[test]
fn jclark_not_wf_sa_052() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/052.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Character data outside elements");
}

#[test]
fn jclark_not_wf_sa_053() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/053.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn jclark_not_wf_sa_054() {
    //PUBLIC require 2 literals
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/054.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_055() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/055.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn jclark_not_wf_sa_056() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/056.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn jclark_not_wf_sa_057() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/057.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn jclark_not_wf_sa_058() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/058.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 3: character \',\' not allowed in attribute definition"
    );
}

#[test]
fn jclark_not_wf_sa_059() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/059.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn jclark_not_wf_sa_060() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/060.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 3: expected \'NOTATION\' at this location"
    );
}

#[test]
fn jclark_not_wf_sa_061() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/061.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_062() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/062.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_063() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/063.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn jclark_not_wf_sa_064() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/064.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_065() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/065.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_066() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/066.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_067() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/067.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_068() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/068.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_069() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/069.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn jclark_not_wf_sa_070() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/070.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Comment improperly terminated; two hyphens (\'--\') is a prohibited sequence in comments"
    );
}

#[test]
fn jclark_not_wf_sa_071() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/071.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: unknown entity \'e2\'");
}

#[test]
fn jclark_not_wf_sa_072() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/072.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 1: unknown entity \'foo\'");
}

#[test]
fn jclark_not_wf_sa_073() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/073.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: unknown entity \'f\'");
}

#[test]
fn jclark_not_wf_sa_074() {
    // entity itself must be well formed
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/074.xml").unwrap_err();
    assert_eq!(e.to_string(), "ENDTAG - Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_075() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/075.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: unknown entity \'e2\'");
}

#[test]
fn jclark_not_wf_sa_076() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/076.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 1: unknown entity \'foo\'");
}

#[test]
fn jclark_not_wf_sa_077() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/077.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: unknown entity \'bar\'");
}

#[test]
fn jclark_not_wf_sa_078() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/078.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: unknown entity \'foo\'");
}

#[test]
fn jclark_not_wf_sa_079() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/079.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: unknown entity \'e2\'");
}

#[test]
fn jclark_not_wf_sa_080() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/080.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: unknown entity \'e2\'");
}

#[test]
fn jclark_not_wf_sa_081() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/081.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_sa_082() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/082.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_sa_083() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/083.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_sa_084() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/084.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_sa_085() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/085.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn jclark_not_wf_sa_086() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/086.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn jclark_not_wf_sa_087() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/087.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn jclark_not_wf_sa_088() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/088.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn jclark_not_wf_sa_089() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/089.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn jclark_not_wf_sa_090() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/090.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn jclark_not_wf_sa_091() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/091.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn jclark_not_wf_sa_092() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/092.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_093() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/093.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn jclark_not_wf_sa_094() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/094.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn jclark_not_wf_sa_095() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/095.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn jclark_not_wf_sa_096() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/096.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn jclark_not_wf_sa_097() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/097.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn jclark_not_wf_sa_098() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/098.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn jclark_not_wf_sa_099() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/099.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn jclark_not_wf_sa_100() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/100.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: when present, standalone must be \'yes\' or \'no\'"
    );
}

#[test]
fn jclark_not_wf_sa_101() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/101.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: whitespace not allowed in encoding name"
    );
}

#[test]
fn jclark_not_wf_sa_102() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/102.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
#[ignore = "Needs inline content expansion of entities"]
fn jclark_not_wf_sa_103() {
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/103.xml").unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn jclark_not_wf_sa_104() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/104.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn jclark_not_wf_sa_105() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/105.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "CDATA not allowed here");
}

#[test]
fn jclark_not_wf_sa_106() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/106.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Character data outside elements");
}

#[test]
fn jclark_not_wf_sa_107() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/107.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn jclark_not_wf_sa_108() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/108.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn jclark_not_wf_sa_109() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/109.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn jclark_not_wf_sa_110() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/110.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn jclark_not_wf_sa_111() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/111.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn jclark_not_wf_sa_112() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/112.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn jclark_not_wf_sa_113() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/113.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_114() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/114.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
#[ignore = "Requires entity expansion"]
fn jclark_not_wf_sa_115() {
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/115.xml").unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires entity expansion"]
fn jclark_not_wf_sa_116() {
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/116.xml").unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires entity expansion"]
fn jclark_not_wf_sa_117() {
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/117.xml").unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn jclark_not_wf_sa_118() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/118.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
#[ignore = "Requires entity expansion"]
fn jclark_not_wf_sa_119() {
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/119.xml").unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires entity expansion"]
fn jclark_not_wf_sa_120() {
    let e = fastxml::parse_file("./tests/xmlconf/xmltest/not-wf/sa/120.xml").unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn jclark_not_wf_sa_121() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/121.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn jclark_not_wf_sa_122() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/122.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn jclark_not_wf_sa_123() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/123.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: expected \'>\' at this location");
}

#[test]
fn jclark_not_wf_sa_124() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/124.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_125() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/125.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_126() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/126.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: expected \'>\' at this location");
}

#[test]
fn jclark_not_wf_sa_127() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/127.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: expected \'>\' at this location");
}

#[test]
fn jclark_not_wf_sa_128() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/128.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn jclark_not_wf_sa_129() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/129.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn jclark_not_wf_sa_130() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/130.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: expected \'>\' at this location");
}

#[test]
fn jclark_not_wf_sa_131() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/131.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: expected \'>\' at this location");
}

#[test]
fn jclark_not_wf_sa_132() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/132.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn jclark_not_wf_sa_133() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/133.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_134() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/134.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: expected \'>\' at this location");
}

#[test]
fn jclark_not_wf_sa_135() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/135.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_136() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/136.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn jclark_not_wf_sa_137() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/137.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_138() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/138.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
#[should_panic]
fn jclark_not_wf_sa_139() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/139.xml").unwrap();
    let _parsed = fastxml::parse_doc(&contents).unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

//
// Tests 140 and 141 not apply since XML 1.0 (Fifth Edition)
//

#[test]
fn jclark_not_wf_sa_142() {
    // invalid character (as numeric entity)
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/142.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_143() {
    // invalid character (as numeric entity)
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/143.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_144() {
    // invalid character (as numeric entity)
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/144.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
#[should_panic]
fn jclark_not_wf_sa_145() {
    // invalid character (as numeric entity)
    // half a surrogate pair blows up the parser!
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/145.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
#[should_panic]
fn jclark_not_wf_sa_146() {
    // invalid character (as numeric entity)
    // this invalid character blows up the parser!
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/146.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_147() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/147.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_148() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/148.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_149() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/149.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_150() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/150.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_151() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/151.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_152() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/152.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn jclark_not_wf_sa_153() {
    // Text declarations may not begin internal parsed entities
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/153.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_154() {
    //PUBLIC require 2 literals
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/154.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_155() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/155.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_156() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/156.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_157() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/157.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn jclark_not_wf_sa_158() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/158.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "ATTLIST name is required");
}

#[test]
fn jclark_not_wf_sa_159() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/159.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_160() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/160.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Parsed entity cannot occur in entity value");
}

#[test]
fn jclark_not_wf_sa_161() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/161.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_162() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/162.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Parsed entity cannot occur in entity value");
}

#[test]
fn jclark_not_wf_sa_163() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/163.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn jclark_not_wf_sa_164() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/164.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character after internal subset declaration"
    );
}

#[test]
fn jclark_not_wf_sa_165() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/165.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 2: missing required whitespace");
}

#[test]
fn jclark_not_wf_sa_166() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/166.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_167() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/167.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
#[should_panic]
fn jclark_not_wf_sa_168() {
    // bad UTF-8
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/168.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[should_panic]
fn jclark_not_wf_sa_169() {
    // bad UTF-8
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/169.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Need to propogate error from decoder"]
fn jclark_not_wf_sa_170() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/170.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_171() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/171.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_172() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/172.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_173() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/173.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_174() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/174.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_175() {
    // Character FFFF is not legal anywhere in an XML document.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/175.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_176() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/176.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_177() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/177.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn jclark_not_wf_sa_178() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/178.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn jclark_not_wf_sa_179() {
    // Invalid syntax matching double quote is missing.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/179.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn jclark_not_wf_sa_180() {
    // The Entity Declared WFC requires entities to be declared before they are used in an attribute list declaration.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/180.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_sa_181() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/181.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
#[ignore = "TODO: verify parsed entity; 4.3.2"]
fn jclark_not_wf_sa_182() {
    // Internal parsed entities must match the <EM>content</EM> production to be well formed.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/182.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn jclark_not_wf_sa_183() {
    // Mixed content declarations may not include content particles.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/183.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_184() {
    // In mixed content models, element names must not be parenthesized.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/184.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn jclark_not_wf_sa_185() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/185.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_sa_186() {
    // Whitespace is required between attribute/value pairs.
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/sa/186.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_001() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/001.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
fn jclark_not_wf_not_sa_002() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/002.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_003() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/003.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_004() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/004.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_005() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/005.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_006() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/006.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_007() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/007.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_008() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/008.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_009() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/009.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_010() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/010.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}
#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn jclark_not_wf_not_sa_011() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/not-sa/011.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
fn jclark_not_wf_ext_sa_001() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/ext-sa/001.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_ext_sa_002() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/ext-sa/002.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: unknown entity \'e\'");
}

#[test]
fn jclark_not_wf_ext_sa_003() {
    let contents = fs::read("./tests/xmlconf/xmltest/not-wf/ext-sa/003.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: unknown entity \'e\'");
}

// invalid (4)
//
// valid/sa (119)
// For this set of tests there is an OUTPUT which is semantically identical.
// Need to see if it is canonical?

#[test]
fn jclark_valid_sa_001() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/001.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_002() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/002.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_003() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/003.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_004() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/004.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_005() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/005.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_006() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/006.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_007() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/007.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_008() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/008.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_009() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/009.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_010() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/010.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_011() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/011.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_012() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/012.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_013() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/013.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_014() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/014.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_015() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/015.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_016() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/016.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_017() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/017.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_018() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/018.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_019() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/019.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_020() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/020.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_021() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/021.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_022() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/022.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_023() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/023.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_024() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/024.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_025() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/025.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_026() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/026.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_027() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/027.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_028() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/028.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_029() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/029.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_030() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/030.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_031() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/031.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_032() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/032.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_033() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/033.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_034() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/034.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_035() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/035.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_036() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/036.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_037() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/037.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_038() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/038.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_039() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/039.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_040() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/040.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_041() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/041.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_042() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/042.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_043() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/043.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_044() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/044.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_045() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/045.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_046() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/046.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_047() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/047.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_048() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/048.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_049() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/049.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_050() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/050.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_051() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/051.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("เจมส์", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_052() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/052.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_053() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/053.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_054() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/054.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_055() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/055.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_056() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/056.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_057() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/057.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_058() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/058.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_059() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/059.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_060() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/060.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_061() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/061.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_062() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/062.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_063() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/063.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("เจมส์", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_064() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/064.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_065() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/065.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_066() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/066.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_067() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/067.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_068() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/068.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_069() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/069.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_070() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/070.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_071() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/071.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_072() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/072.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_073() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/073.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_074() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/074.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_075() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/075.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_076() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/076.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_077() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/077.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_078() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/078.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_079() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/079.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_080() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/080.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_081() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/081.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_082() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/082.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_083() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/083.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_084() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/084.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_085() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/085.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_086() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/086.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_087() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/087.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_088() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/088.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_089() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/089.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_090() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/090.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_091() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/091.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_092() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/092.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_093() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/093.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_094() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/094.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_095() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/095.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_096() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/096.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Required external entity resolution at parse time"]
fn jclark_valid_sa_097() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/097.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_098() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/098.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_099() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/099.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_100() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/100.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_101() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/101.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_102() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/102.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_103() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/103.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_104() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/104.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_105() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/105.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_106() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/106.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_107() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/107.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_108() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/108.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_109() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/109.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_110() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/110.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_111() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/111.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_112() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/112.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_113() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/113.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "TODO: handle declared entities"]
fn jclark_valid_sa_114() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/114.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "TODO: handle declared entities"]
fn jclark_valid_sa_115() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/115.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_116() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/116.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_117() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/117.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_118() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/118.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn jclark_valid_sa_119() {
    let contents = fs::read("./tests/xmlconf/xmltest/valid/sa/119.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

//
// valid/not-sa (031)
//
// valid/ext-sa (14)
