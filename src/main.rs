extern crate fastxml;

use std::env;
use std::error::Error;
use std::fs;
use std::process;
use std::time::Instant;

fn main() {
    let args: Vec<String> = env::args().collect();
    dbg!(&args);

    let filename = parse_args(&args).unwrap();

    if let Err(e) = parse_file(filename) {
        eprintln!("Application error: {}", e);
        process::exit(1)
    }
    // create cursor
    // select and eval XPath
    // sample usage
    // let p = XmlParser::new()
    //  .ignore_dtd()
    //  .with_catalog(xmlcatalog)
    //  .resolve_local_entities(allowed_dirs)
    //  .resolve_network_entities(allowed_hosts)
    //  .parse_document()
    //  .validate_dtd() <ValidatedDocument, Error>

    // entities
    //  contains_lt
    //  external|internal
    //  expanded_form
}

fn parse_args(args: &[String]) -> Result<&str, &str> {
    if args.len() < 2 {
        return Err("Need a filename");
    }
    let filename = &args[1];
    Ok(filename)
}

fn parse_file(filename: &str) -> Result<(), Box<dyn Error>> {
    let contents = fs::read(filename)?;

    let instant = Instant::now();
    let parsed = fastxml::parse_doc(&contents)?;
    let dur = instant.elapsed();
    println!(
        "\nRoot:\n{:?} has val {:?}",
        parsed.root,
        parsed.token_val(parsed.root)
    );
    println!("Parsed {} records in {:?}", parsed.records.len(), dur);

    println!("{:?}", parsed.records);

    Ok(())
}
