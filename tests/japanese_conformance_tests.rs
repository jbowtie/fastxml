extern crate fastxml;

use std::fs;

// Fuji Xerox Japanese Text Tests XML 1.0 Tests
#[test]
fn japanese_prxml_euc_jp() {
    let contents = fs::read("./tests/xmlconf/japanese/pr-xml-euc-jp.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("spec", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Complains about invalid character in start tag - encoding problem?"]
fn japanese_prxml_iso_2022_jp() {
    let contents = fs::read("./tests/xmlconf/japanese/pr-xml-iso-2022-jp.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("spec", parsed.token_val(parsed.root));
}

#[test]
fn japanese_prxml_little_endian() {
    let contents = fs::read("./tests/xmlconf/japanese/pr-xml-little-endian.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("spec", parsed.token_val(parsed.root));
}

#[test]
fn japanese_prxml_shift_jis() {
    let contents = fs::read("./tests/xmlconf/japanese/pr-xml-shift_jis.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("spec", parsed.token_val(parsed.root));
}

#[test]
fn japanese_prxml_utf16() {
    let contents = fs::read("./tests/xmlconf/japanese/pr-xml-utf-16.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("spec", parsed.token_val(parsed.root));
}

#[test]
fn japanese_prxml_utf8() {
    let contents = fs::read("./tests/xmlconf/japanese/pr-xml-utf-8.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("spec", parsed.token_val(parsed.root));
}

#[test]
fn japanese_weekly_euc_jp() {
    let contents = fs::read("./tests/xmlconf/japanese/weekly-euc-jp.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("週報", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Complains about invalid character in start tag - encoding problem?"]
fn japanese_weekly_iso_2022_jp() {
    let contents = fs::read("./tests/xmlconf/japanese/weekly-iso-2022-jp.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("週報", parsed.token_val(parsed.root));
}

#[test]
fn japanese_weekly_little_endian() {
    let contents = fs::read("./tests/xmlconf/japanese/weekly-little-endian.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("週報", parsed.token_val(parsed.root));
}

#[test]
fn japanese_weekly_shift_jis() {
    let contents = fs::read("./tests/xmlconf/japanese/weekly-shift_jis.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("週報", parsed.token_val(parsed.root));
}

#[test]
fn japanese_weekly_utf16() {
    let contents = fs::read("./tests/xmlconf/japanese/weekly-utf-16.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("週報", parsed.token_val(parsed.root));
}

#[test]
fn japanese_weekly_utf8() {
    let contents = fs::read("./tests/xmlconf/japanese/weekly-utf-8.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("週報", parsed.token_val(parsed.root));
}
