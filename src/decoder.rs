use encoding_rs::Encoding;
use encoding_rs::UTF_8;
use std::convert::TryInto;
use std::io::{ErrorKind, Read, Seek, SeekFrom};
use std::slice;

// https://tools.ietf.org/html/rfc3629
static UTF8_CHAR_WIDTH: [u8; 256] = [
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, // 0x1F
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, // 0x3F
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, // 0x5F
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, // 0x7F
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, // 0x9F
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, // 0xBF
    0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, // 0xDF
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // 0xEF
    4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0xFF
];

pub struct XmlDecoder<R> {
    inner: R,
    pub encoder: &'static Encoding,
}

#[derive(Debug, PartialEq)]
pub struct DecodedChar(pub char, pub usize);

impl<R: Read + Seek> XmlDecoder<R> {
    pub fn new(bytes: R, encoder: &'static Encoding) -> Self {
        XmlDecoder {
            inner: bytes,
            encoder,
        }
    }

    pub fn advance(&mut self, n: usize) {
        for _ in 0..n {
            self.next_byte();
        }
    }

    fn next_byte(&mut self) -> Option<std::io::Result<u8>> {
        let mut byte = 0;
        loop {
            return match self.inner.read(slice::from_mut(&mut byte)) {
                Ok(0) => None,
                Ok(..) => Some(Ok(byte)),
                Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
                Err(e) => Some(Err(e)),
            };
        }
    }

    // use BufReader: seek_relative to avoid flushing buffer everytime
    // mostly useful for reverting offset when trying to match a string
    pub fn seek(&mut self, n: usize) {
        self.inner
            .seek(SeekFrom::Start(n.try_into().unwrap()))
            .unwrap();
    }
}

impl<R: Read + Seek> Iterator for XmlDecoder<R> {
    type Item = DecodedChar;

    fn next(&mut self) -> Option<DecodedChar> {
        // get the next byte from the underlying stream
        let b = self.next_byte();
        b.as_ref()?;
        // turn it into an input vector (in case it is multibyte)
        let b = b.unwrap().unwrap();
        let mut input = vec![b];

        // optimize for UTF-8
        if self.encoder == UTF_8 {
            let width = UTF8_CHAR_WIDTH[b as usize] as usize;
            while input.len() < width {
                let b2 = self.next_byte().unwrap().unwrap();
                input.push(b2);
            }
            let c = std::str::from_utf8(&input).unwrap().chars().next().unwrap();
            return Some(DecodedChar(c, width));
        }

        // handle single byte
        if self.encoder.is_single_byte() {
            let (output, _) = self.encoder.decode_without_bom_handling(&input[..]);
            return Some(DecodedChar(output.as_ref().chars().next().unwrap(), 1));
        }

        // for variable-length encodings
        let mut buffer_bytes = [0u8; 8];
        let buffer: &mut str = std::str::from_utf8_mut(&mut buffer_bytes[..]).unwrap();
        let mut offset = 0;
        let mut decoder = self.encoder.new_decoder();
        // we can feed one byte at a time to the decoder
        // until we get a valid character, so we'd know the width
        let width = loop {
            let (_result, _read, written) =
                decoder.decode_to_str_without_replacement(&input[offset..], buffer, false);
            if written > 0 {
                break input.len();
            }
            // TODO: handle None
            let b2 = self.next_byte().unwrap().unwrap();
            input.push(b2);
            offset += 1;
        };
        Some(DecodedChar(buffer.chars().next().unwrap(), width))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use encoding_rs::{UTF_16LE, UTF_8};
    use std::io::{BufReader, Cursor};

    #[test]
    fn decode_utf8() {
        let s = "<foo\u{269b} />";
        let c = Cursor::new(s);
        let b = BufReader::new(c);
        let mut d = XmlDecoder::new(b, UTF_8).peekable();

        //next - advance per usual
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('<', 1), z);

        //peek - no advancement
        let z = d.peek().unwrap();
        assert_eq!(&DecodedChar('f', 1), z);
        //next - next char
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('f', 1), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('o', 1), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('o', 1), z);
        // multi-byte character should just work
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('⚛', 3), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar(' ', 1), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('/', 1), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('>', 1), z);
        // check that we get None after last character
        let z = d.next();
        assert_eq!(None, z);
    }

    #[test]
    fn decode_utf16le() {
        let s = b"<\0f\0o\0o\0 \0/\0>\0";
        let c = Cursor::new(s);
        let b = BufReader::new(c);
        let mut d = XmlDecoder::new(b, UTF_16LE).peekable();
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('<', 2), z);
        let z = d.peek().unwrap();
        assert_eq!(&DecodedChar('f', 2), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('f', 2), z);
        let z = d.next().unwrap();
        assert_eq!(DecodedChar('o', 2), z);
    }
}
