extern crate fastxml;

use std::fs;

// IBM XML 1.0 Tests

// invalid cases
#[test]
fn ibm_not_wf_p01_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P01/ibm01n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Character data outside elements");
}

#[test]
fn ibm_not_wf_p01_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P01/ibm01n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p01_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P01/ibm01n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn ibm_not_wf_p02_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n10() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n11() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n12() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n13() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n14() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n15() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n15.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n16() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n16.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n17() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n17.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n18() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n18.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n19() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n19.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n20() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n20.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n21() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n21.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n22() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n22.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n23() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n23.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n24() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n24.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n25() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n25.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n26() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n26.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n27() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n27.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n28() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n28.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n29() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n29.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
#[should_panic]
fn ibm_not_wf_p02_n30() {
    // invalid UTF-8
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n30.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
#[should_panic]
fn ibm_not_wf_p02_n31() {
    // invalid UTF-8
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n31.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n32() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n32.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p02_n33() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P02/ibm02n33.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p03_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P03/ibm03n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag not properly formed");
}

#[test]
fn ibm_not_wf_p04_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n10() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n11() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p04_n12() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n13() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n14() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n15() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n15.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n16() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n16.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n17() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n17.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p04_n18() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P04/ibm04n18.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE name");
}

#[test]
fn ibm_not_wf_p05_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P05/ibm05n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn ibm_not_wf_p05_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P05/ibm05n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn ibm_not_wf_p05_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P05/ibm05n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn ibm_not_wf_p09_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P09/ibm09n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Parsed entity cannot occur in entity value");
}

#[test]
fn ibm_not_wf_p09_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P09/ibm09n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 4: character \'\"\' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p09_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P09/ibm09n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p09_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P09/ibm09n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'FullName\'");
}

#[test]
fn ibm_not_wf_p10_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn ibm_not_wf_p10_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 14: character \'\"\' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p10_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
fn ibm_not_wf_p10_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn ibm_not_wf_p10_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn ibm_not_wf_p10_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 14: character \'\'\' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p10_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
fn ibm_not_wf_p10_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P10/ibm10n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn ibm_not_wf_p11_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P11/ibm11n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p11_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P11/ibm11n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p11_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P11/ibm11n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p11_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P11/ibm11n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p12_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P12/ibm12n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p12_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P12/ibm12n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p12_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P12/ibm12n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p13_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P13/ibm13n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p13_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P13/ibm13n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p13_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P13/ibm13n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p14_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P14/ibm14n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn ibm_not_wf_p14_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P14/ibm14n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p14_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P14/ibm14n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 9: character \' \' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p15_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P15/ibm15n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Comment improperly terminated; two hyphens (\'--\') is a prohibited sequence in comments"
    );
}

#[test]
fn ibm_not_wf_p15_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P15/ibm15n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p15_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P15/ibm15n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p15_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P15/ibm15n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p16_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P16/ibm16n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Character data outside elements");
}

#[test]
fn ibm_not_wf_p16_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P16/ibm16n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid PI tag");
}

#[test]
fn ibm_not_wf_p16_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P16/ibm16n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p16_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P16/ibm16n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p17_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P17/ibm17n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p17_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P17/ibm17n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p17_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P17/ibm17n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p17_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P17/ibm17n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p18_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P18/ibm18n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid CDATA section close found in text");
}

#[test]
fn ibm_not_wf_p18_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P18/ibm18n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p19_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P19/ibm19n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p19_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P19/ibm19n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p19_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P19/ibm19n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid PI tag");
}

#[test]
fn ibm_not_wf_p20_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P20/ibm20n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "CDATA not allowed here");
}

#[test]
fn ibm_not_wf_p21_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P21/ibm21n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p21_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P21/ibm21n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p21_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P21/ibm21n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "CDATA not allowed here");
}

#[test]
fn ibm_not_wf_p22_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P22/ibm22n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p22_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P22/ibm22n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p22_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P22/ibm22n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p23_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P23/ibm23n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn ibm_not_wf_p23_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P23/ibm23n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn ibm_not_wf_p23_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P23/ibm23n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn ibm_not_wf_p23_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P23/ibm23n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p23_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P23/ibm23n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn ibm_not_wf_p23_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P23/ibm23n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p24_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected opening quote)"
    );
}

#[test]
fn ibm_not_wf_p24_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 1: missing required whitespace");
}

#[test]
fn ibm_not_wf_p24_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn ibm_not_wf_p24_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn ibm_not_wf_p24_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn ibm_not_wf_p24_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn ibm_not_wf_p24_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration must have version as first attribute"
    );
}

#[test]
fn ibm_not_wf_p24_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn ibm_not_wf_p24_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P24/ibm24n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: invalid version closing quote"
    );
}

#[test]
fn ibm_not_wf_p25_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P25/ibm25n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected opening quote)"
    );
}

#[test]
fn ibm_not_wf_p25_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P25/ibm25n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn ibm_not_wf_p26_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P26/ibm26n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration: unexpected version");
}

#[test]
fn ibm_not_wf_p27_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P27/ibm27n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn ibm_not_wf_p28_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p28_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character after internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p28_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p28_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p28_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p28_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p28_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p28_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P28/ibm28n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
#[ignore = "Requires handling external DTD to trigger"]
fn ibm_not_wf_p28a_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/p28a/ibm28an01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p29_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn ibm_not_wf_p29_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn ibm_not_wf_p29_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn ibm_not_wf_p29_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Parsed entity cannot occur in entity value");
}

#[test]
fn ibm_not_wf_p29_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p29_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p29_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P29/ibm29n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn ibm_not_wf_p30_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P30/ibm30n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Character data outside elements");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn ibm_not_wf_p31_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P31/ibm31n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Character data outside elements");
}

#[test]
fn ibm_not_wf_p32_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn ibm_not_wf_p32_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn ibm_not_wf_p32_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn ibm_not_wf_p32_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: when present, standalone must be \'yes\' or \'no\'"
    );
}

#[test]
fn ibm_not_wf_p32_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: when present, standalone must be \'yes\' or \'no\'"
    );
}

#[test]
fn ibm_not_wf_p32_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: when present, standalone must be \'yes\' or \'no\'"
    );
}

#[test]
fn ibm_not_wf_p32_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: when present, standalone must be \'yes\' or \'no\'"
    );
}

#[test]
fn ibm_not_wf_p32_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn ibm_not_wf_p32_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P32/ibm32n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 9: unknown entity \'animal_content\'");
}

#[test]
fn ibm_not_wf_p39_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P39/ibm39n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p39_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P39/ibm39n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p39_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P39/ibm39n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p39_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P39/ibm39n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "ENDTAG - Document not terminated properly");
}

#[test]
fn ibm_not_wf_p39_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P39/ibm39n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "ENDTAG - Document not terminated properly");
}

#[test]
fn ibm_not_wf_p39_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P39/ibm39n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Document not terminated properly");
}

#[test]
fn ibm_not_wf_p40_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P40/ibm40n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p40_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P40/ibm40n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p40_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P40/ibm40n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p40_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P40/ibm40n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p40_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P40/ibm40n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "duplicate attribute");
}

#[test]
fn ibm_not_wf_p41_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p41_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid char in attribute");
}

#[test]
fn ibm_not_wf_p41_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Attribute value: unexpected character (expected opening quote)"
    );
}

#[test]
fn ibm_not_wf_p41_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p41_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid char in attribute");
}

#[test]
fn ibm_not_wf_p41_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p41_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid char in attribute");
}

#[test]
fn ibm_not_wf_p41_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p41_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p41_n10() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'aExternal\'");
}

#[test]
fn ibm_not_wf_p41_n11() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: unknown entity \'aExternal\'");
}

#[test]
fn ibm_not_wf_p41_n12() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 9: unknown entity \'aImage\'");
}

#[test]
fn ibm_not_wf_p41_n13() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
#[ignore = "TODO: verify parsed entity; 4.3.2"]
fn ibm_not_wf_p41_n14() {
    // attribute indirectly contains LT via entity expansion
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P41/ibm41n14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn ibm_not_wf_p42_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P42/ibm42n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn ibm_not_wf_p42_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P42/ibm42n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn ibm_not_wf_p42_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P42/ibm42n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn ibm_not_wf_p42_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P42/ibm42n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag does not match start tag");
}

#[test]
fn ibm_not_wf_p42_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P42/ibm42n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "End tag not properly formed");
}

#[test]
fn ibm_not_wf_p43_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P43/ibm43n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p43_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P43/ibm43n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p43_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P43/ibm43n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p43_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P43/ibm43n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unrecognized sequence after <!");
}

#[test]
fn ibm_not_wf_p44_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P44/ibm44n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after <");
}

#[test]
fn ibm_not_wf_p44_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P44/ibm44n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p44_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P44/ibm44n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in start tag");
}

#[test]
fn ibm_not_wf_p44_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P44/ibm44n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "duplicate attribute");
}

#[test]
fn ibm_not_wf_p45_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn ibm_not_wf_p45_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn ibm_not_wf_p45_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p45_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn ibm_not_wf_p45_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn ibm_not_wf_p45_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn ibm_not_wf_p45_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p45_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p45_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P45/ibm45n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p46_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P46/ibm46n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p46_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P46/ibm46n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p46_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P46/ibm46n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p46_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P46/ibm46n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p46_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P46/ibm46n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p47_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P47/ibm47n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p47_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P47/ibm47n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p47_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P47/ibm47n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p47_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P47/ibm47n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p47_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P47/ibm47n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn ibm_not_wf_p47_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P47/ibm47n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: expected \'>\' at this location");
}

#[test]
fn ibm_not_wf_p48_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p48_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p48_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p48_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p48_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p48_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p48_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P48/ibm48n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p49_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P49/ibm49n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p49_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P49/ibm49n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
#[ignore = "INVESTIGATE"]
fn ibm_not_wf_p49_n03() {
    // invalid seperator in entity_decl
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P49/ibm49n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p49_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P49/ibm49n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p49_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P49/ibm49n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p49_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P49/ibm49n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p50_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p50_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p50_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn ibm_not_wf_p50_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p50_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p50_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: expected \'>\' at this location");
}

#[test]
fn ibm_not_wf_p50_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P50/ibm50n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p51_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p51_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p51_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn ibm_not_wf_p51_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn ibm_not_wf_p51_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: expected \'*\' at this location");
}

#[test]
fn ibm_not_wf_p51_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn ibm_not_wf_p51_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P51/ibm51n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in mixed content declaration"
    );
}

#[test]
fn ibm_not_wf_p52_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P52/ibm52n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p52_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P52/ibm52n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p52_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P52/ibm52n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p52_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P52/ibm52n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p52_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P52/ibm52n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn ibm_not_wf_p52_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P52/ibm52n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p53_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p53_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p53_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p53_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p53_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn ibm_not_wf_p53_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p53_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn ibm_not_wf_p53_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P53/ibm53n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn ibm_not_wf_p54_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P54/ibm54n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p54_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P54/ibm54n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p55_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P55/ibm55n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p55_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P55/ibm55n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p55_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P55/ibm55n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: expected \'CDATA\' at this location");
}

#[test]
fn ibm_not_wf_p56_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p56_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: expected \'ID\' at this location");
}

#[test]
fn ibm_not_wf_p56_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: expected \'ID\' at this location");
}

#[test]
fn ibm_not_wf_p56_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p56_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p56_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn ibm_not_wf_p56_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P56/ibm56n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p57_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P57/ibm57n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p58_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p58_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: expected \'(\' at this location");
}

#[test]
fn ibm_not_wf_p58_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NAME in attribute definition");
}

#[test]
fn ibm_not_wf_p58_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 8: character \'#\' not allowed in attribute definition"
    );
}

#[test]
fn ibm_not_wf_p58_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p58_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 10: character \',\' not allowed in attribute definition"
    );
}

#[test]
fn ibm_not_wf_p58_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 9: missing required whitespace");
}

#[test]
fn ibm_not_wf_p58_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P58/ibm58n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NAME in attribute definition");
}

#[test]
fn ibm_not_wf_p59_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P59/ibm59n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NMTOKEN in attribute definition");
}

#[test]
fn ibm_not_wf_p59_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P59/ibm59n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 8: character \'#\' not allowed in attribute definition"
    );
}

#[test]
fn ibm_not_wf_p59_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P59/ibm59n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 9: character \',\' not allowed in attribute definition"
    );
}

#[test]
fn ibm_not_wf_p59_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P59/ibm59n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid NMTOKEN in attribute definition");
}

#[test]
fn ibm_not_wf_p59_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P59/ibm59n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p59_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P59/ibm59n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unknown attribute type in definition");
}

#[test]
fn ibm_not_wf_p60_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p60_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p60_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn ibm_not_wf_p60_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Attribute default: unexpected character (expected opening quote)"
    );
}

#[test]
fn ibm_not_wf_p60_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: missing required whitespace");
}

#[test]
fn ibm_not_wf_p60_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
fn ibm_not_wf_p60_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Attribute value cannot contain < character");
}

#[test]
fn ibm_not_wf_p60_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P60/ibm60n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character in ATTLIST definition");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p61_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P61/ibm61n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p62_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P62/ibm62n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p63_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P63/ibm63n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p64_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P64/ibm64n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p64_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P64/ibm64n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p64_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P64/ibm64n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p65_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P65/ibm65n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
#[ignore = "Requires external DTD support to trigger"]
fn ibm_not_wf_p65_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P65/ibm65n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "");
}

#[test]
fn ibm_not_wf_p66_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn ibm_not_wf_p66_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
fn ibm_not_wf_p66_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn ibm_not_wf_p66_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn ibm_not_wf_p66_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
fn ibm_not_wf_p66_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
fn ibm_not_wf_p66_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
fn ibm_not_wf_p66_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn ibm_not_wf_p66_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
fn ibm_not_wf_p66_n10() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid numeric entity");
}

#[test]
fn ibm_not_wf_p66_n11() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid hex entity");
}

#[test]
fn ibm_not_wf_p66_n12() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p66_n13() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p66_n14() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n14.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p66_n15() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P66/ibm66n15.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid character");
}

#[test]
fn ibm_not_wf_p68_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn ibm_not_wf_p68_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: character \'\"\' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p68_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid entity found");
}

#[test]
fn ibm_not_wf_p68_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: unknown entity \'aAa\'");
}

#[test]
fn ibm_not_wf_p68_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'aaa\'");
}

#[test]
fn ibm_not_wf_p68_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'aaa\'");
}

#[test]
fn ibm_not_wf_p68_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'aaa\'");
}

#[test]
fn ibm_not_wf_p68_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 9: unknown entity \'aImage\'");
}

#[test]
fn ibm_not_wf_p68_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'bbb\'");
}

#[test]
fn ibm_not_wf_p68_n10() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P68/ibm68n10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'bbb\'");
}

#[test]
fn ibm_not_wf_p69_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'\'");
}

#[test]
fn ibm_not_wf_p69_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 6: character \'\r\' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p69_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 6: character \' \' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p69_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 6: character \' \' not allowed  in entity name"
    );
}

#[test]
fn ibm_not_wf_p69_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'paaa\'");
}

#[test]
fn ibm_not_wf_p69_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: unknown entity \'bbb\'");
}

#[test]
fn ibm_not_wf_p69_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P69/ibm69n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: unknown entity \'bbb\'");
}

#[test]
fn ibm_not_wf_p71_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p71_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p71_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p71_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn ibm_not_wf_p71_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn ibm_not_wf_p71_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p71_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p71_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P71/ibm71n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p72_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p72_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn ibm_not_wf_p72_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p72_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p72_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn ibm_not_wf_p72_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "invalid name token");
}

#[test]
fn ibm_not_wf_p72_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p72_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p72_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P72/ibm72n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p73_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P73/ibm73n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn ibm_not_wf_p73_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P73/ibm73n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn ibm_not_wf_p74_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P74/ibm74n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p75_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn ibm_not_wf_p75_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p75_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 4: expected \'PUBLIC\' at this location"
    );
}

#[test]
fn ibm_not_wf_p75_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p75_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: missing required whitespace");
}

#[test]
fn ibm_not_wf_p75_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn ibm_not_wf_p75_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn ibm_not_wf_p75_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p75_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn ibm_not_wf_p75_n10() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn ibm_not_wf_p75_n11() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p75_n12() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p75_n13() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P75/ibm75n13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character after DOCTYPE declaration");
}

#[test]
fn ibm_not_wf_p76_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p76_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: expected \'NDATA\' at this location");
}

#[test]
fn ibm_not_wf_p76_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p76_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p76_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: missing required whitespace");
}

#[test]
fn ibm_not_wf_p76_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: missing required whitespace");
}

#[test]
fn ibm_not_wf_p76_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P76/ibm76n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of entity definition)");
}

#[test]
fn ibm_not_wf_p77_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P77/ibm77n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'aExternal\'");
}

#[test]
fn ibm_not_wf_p77_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P77/ibm77n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'aExternal\'");
}

#[test]
fn ibm_not_wf_p77_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P77/ibm77n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: unknown entity \'pExternal\'");
}

#[test]
fn ibm_not_wf_p77_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P77/ibm77n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: unknown entity \'pExternal\'");
}

#[test]
fn ibm_not_wf_p78_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P78/ibm78n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'aExternal\'");
}

#[test]
fn ibm_not_wf_p78_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P78/ibm78n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: unknown entity \'aExternal\'");
}

#[test]
fn ibm_not_wf_p79_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P79/ibm79n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: unknown entity \'pExternal\'");
}

#[test]
fn ibm_not_wf_p79_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P79/ibm79n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: unknown entity \'pExternal\'");
}

#[test]
fn ibm_not_wf_p80_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P80/ibm80n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: expected whitespace between attributes"
    );
}

#[test]
fn ibm_not_wf_p80_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P80/ibm80n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected \'=\')"
    );
}

#[test]
fn ibm_not_wf_p80_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P80/ibm80n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: unexpected character (expected opening quote)"
    );
}

#[test]
fn ibm_not_wf_p80_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P80/ibm80n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn ibm_not_wf_p80_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P80/ibm80n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn ibm_not_wf_p80_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P80/ibm80n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML declaration not terminated properly");
}

#[test]
fn ibm_not_wf_p81_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p81_n09() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P81/ibm81n09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn ibm_not_wf_p82_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: missing required whitespace");
}

#[test]
fn ibm_not_wf_p82_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected SYSTEM or PUBLIC notation");
}

#[test]
fn ibm_not_wf_p82_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected SYSTEM or PUBLIC notation");
}

#[test]
fn ibm_not_wf_p82_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected SYSTEM or PUBLIC notation");
}

#[test]
fn ibm_not_wf_p82_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p82_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of notation definition)");
}

#[test]
fn ibm_not_wf_p82_n07() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn ibm_not_wf_p82_n08() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P82/ibm82n08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected \'>\' (end of notation definition)");
}

#[test]
fn ibm_not_wf_p83_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P83/ibm83n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected SYSTEM or PUBLIC notation");
}

#[test]
fn ibm_not_wf_p83_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P83/ibm83n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "XML not starting correctly");
}

#[test]
fn ibm_not_wf_p83_n03() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P83/ibm83n03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected SYSTEM or PUBLIC notation");
}

#[test]
fn ibm_not_wf_p83_n04() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P83/ibm83n04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 7: missing required whitespace");
}

#[test]
fn ibm_not_wf_p83_n05() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P83/ibm83n05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn ibm_not_wf_p83_n06() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P83/ibm83n06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected SYSTEM or PUBLIC notation");
}

#[test]
fn ibm_not_wf_p85_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P85/ibm85n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid PI tag");
}

#[test]
fn ibm_not_wf_p85_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P85/ibm85n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid PI tag");
}

//
// The remainder of P85, all of P86 and P87 do not apply since XML 1.0 (Fifth Edition)
//

#[test]
fn ibm_not_wf_p88_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P88/ibm88n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn ibm_not_wf_p88_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P88/ibm88n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

//
// The remainder of P88 does not apply since XML 1.0 (Fifth Edition)
//

#[test]
fn ibm_not_wf_p89_n01() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P89/ibm89n01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

#[test]
fn ibm_not_wf_p89_n02() {
    let contents = fs::read("./tests/xmlconf/ibm/not-wf/P89/ibm89n02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: missing required whitespace");
}

//
// The remainder of P89 does not apply since XML 1.0 (Fifth Edition)
//

// Valid cases
#[test]
fn ibm_valid_p01_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P01/ibm01v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p02_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P02/ibm02v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p03_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P03/ibm03v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p09_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P09/ibm09v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p09_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P09/ibm09v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires external dtd"]
fn ibm_valid_p09_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P09/ibm09v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p09_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P09/ibm09v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires external dtd"]
fn ibm_valid_p09_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P09/ibm09v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v06() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v06.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v07() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v07.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p10_v08() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P10/ibm10v08.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p11_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P11/ibm11v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p11_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P11/ibm11v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p11_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P11/ibm11v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p11_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P11/ibm11v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p12_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P12/ibm12v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p12_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P12/ibm12v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p12_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P12/ibm12v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p12_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P12/ibm12v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p13_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P13/ibm13v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p14_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P14/ibm14v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p14_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P14/ibm14v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p14_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P14/ibm14v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p15_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P15/ibm15v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p15_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P15/ibm15v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p15_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P15/ibm15v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p15_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P15/ibm15v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p16_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P16/ibm16v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p16_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P16/ibm16v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p16_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P16/ibm16v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p17_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P17/ibm17v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p18_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P18/ibm18v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p19_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P19/ibm19v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p20_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P20/ibm20v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p20_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P20/ibm20v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p21_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P21/ibm21v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("student", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v06() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v06.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p22_v07() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P22/ibm22v07.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p23_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P23/ibm23v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p23_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P23/ibm23v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p23_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P23/ibm23v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p23_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P23/ibm23v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p23_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P23/ibm23v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p23_v06() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P23/ibm23v06.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p24_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P24/ibm24v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p24_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P24/ibm24v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p25_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P25/ibm25v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p25_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P25/ibm25v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p25_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P25/ibm25v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p25_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P25/ibm25v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p26_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P26/ibm26v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p27_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P27/ibm27v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p27_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P27/ibm27v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p27_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P27/ibm27v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("doc", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p28_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P28/ibm28v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p28_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P28/ibm28v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p29_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P29/ibm29v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p29_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P29/ibm29v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p30_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P30/ibm30v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p30_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P30/ibm30v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p31_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P31/ibm31v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p32_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P32/ibm32v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Handle external DTD"]
fn ibm_valid_p32_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P32/ibm32v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p32_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P32/ibm32v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p32_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P32/ibm32v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p33_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P33/ibm33v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p34_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P34/ibm34v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p35_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P35/ibm35v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p36_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P36/ibm36v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p37_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P37/ibm37v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p38_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P38/ibm38v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p39_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P39/ibm39v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p40_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P40/ibm40v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p41_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P41/ibm41v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p42_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P42/ibm42v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p43_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P43/ibm43v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p44_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P44/ibm44v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p45_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P45/ibm45v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p47_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P47/ibm47v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p49_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P49/ibm49v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p50_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P50/ibm50v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p51_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P51/ibm51v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p51_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P51/ibm51v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p52_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P52/ibm52v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p54_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P54/ibm54v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p54_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P54/ibm54v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p54_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P54/ibm54v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("AttrType", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p55_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P55/ibm55v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("StType", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("tokenizer", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("tokenizer", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("tokenizer", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("tokenizer", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v06() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v06.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v07() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v07.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v08() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v08.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v09() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v09.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p56_v10() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P56/ibm56v10.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p57_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P57/ibm57v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p58_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P58/ibm58v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p58_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P58/ibm58v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p59_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P59/ibm59v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p59_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P59/ibm59v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p60_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P60/ibm60v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("Java", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p60_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P60/ibm60v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("Java", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p60_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P60/ibm60v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("Java", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p60_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P60/ibm60v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("test", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p61_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P61/ibm61v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p61_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P61/ibm61v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p62_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P62/ibm62v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p62_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P62/ibm62v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p62_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P62/ibm62v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p62_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P62/ibm62v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p62_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P62/ibm62v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p63_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P63/ibm63v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p63_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P63/ibm63v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p63_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P63/ibm63v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p63_v04() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P63/ibm63v04.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p63_v05() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P63/ibm63v05.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p64_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P64/ibm64v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p64_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P64/ibm64v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p64_v03() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P64/ibm64v03.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p65_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P65/ibm65v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p65_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P65/ibm65v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p66_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P66/ibm66v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p67_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P67/ibm67v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p68_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P68/ibm68v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires parsed external entity"]
fn ibm_valid_p68_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P68/ibm68v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p69_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P69/ibm69v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires parsed external entity"]
fn ibm_valid_p69_v02() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P69/ibm69v02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires parsed external entity"]
fn ibm_valid_p70_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P70/ibm70v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires parsed external entity"]
fn ibm_valid_p78_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P78/ibm78v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "Requires parsed external entity"]
fn ibm_valid_p79_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P79/ibm79v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("animal", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p82_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P82/ibm82v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p85_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P85/ibm85v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p86_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P86/ibm86v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p87_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P87/ibm87v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p88_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P88/ibm88v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}

#[test]
fn ibm_valid_p89_v01() {
    let contents = fs::read("./tests/xmlconf/ibm/valid/P89/ibm89v01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("book", parsed.token_val(parsed.root));
}
