use std::char;
use std::rc::Rc;
use std::str;

use encoding_rs::Encoding;

use crate::TokenType;
use crate::VtdRecord;

/// This represents the parsed XML structure
///
/// This includes the VTD records corresponding to the DOM, the byte buffer those records point to,
/// the index of the VTD record that points to the root element, and the detected encoding.
///
/// In future we'll include the location index for even faster reads.
#[derive(Debug)]
pub struct ParsedXml {
    pub records: Vec<VtdRecord>,
    //pub bytes: &'a [u8],
    pub bytes: Rc<[u8]>,
    pub encoding: &'static Encoding,
    //cache: Vec<Vec<ElementLocation>>, // (location index)
    pub root: usize,
}

// basic functions
impl ParsedXml {
    /// Return the token value (without resolving entities)
    pub fn raw_token_val(&self, index: usize) -> String {
        let record = &self.records[index];
        let len = u64::from(record.length);
        let start = record.offset as usize;
        let end = (record.offset + len) as usize;

        let (output, _) = self
            .encoding
            .decode_without_bom_handling(&self.bytes[start..end]);
        output.to_string()
    }

    /// Return the token value (this handles entity replacement)
    pub fn token_val(&self, index: usize) -> String {
        let raw_text = self.raw_token_val(index);
        let record = &self.records[index];
        if (record.kind == TokenType::CharData || record.kind == TokenType::AttrVal)
            && raw_text.find('&').is_some()
        {
            return self.resolve_entities(&raw_text);
        }
        raw_text
    }

    fn resolve_entities(&self, input: &str) -> String {
        let mut s = String::with_capacity(input.len());
        let parts: Vec<&str> = input.split('&').collect();
        s.push_str(parts[0]);
        for p in parts[1..].iter() {
            let z = p.find(';');
            if z.is_none() {
                s.push_str(p);
                continue;
            }
            let (entity, rest) = p.split_at(z.unwrap());
            // there are three kinds of entities to deal with:
            //   predefined entities
            //   numeric entities (decimal and hex)
            //   DTD-defined entities (TODO)
            let replacement = match entity {
                z if z.starts_with("#x") => {
                    char::from_u32(u32::from_str_radix(&z[2..], 16).unwrap())
                        .unwrap()
                        .to_string()
                }
                z if z.starts_with('#') => char::from_u32(z[1..].parse::<u32>().unwrap())
                    .unwrap()
                    .to_string(),
                "amp" => "&".to_string(),
                "lt" => "<".to_string(),
                "gt" => ">".to_string(),
                "apos" => "'".to_string(),
                "quot" => "\"".to_string(),
                _ => "__ERROR__".to_string(),
            };
            s.push_str(&replacement);
            s.push_str(&rest[1..]);
        }
        s
    }

    /// Test to see if the record at this index is an element
    pub fn is_element(&self, index: usize) -> bool {
        let record = &self.records[index];
        record.kind == TokenType::StartTag
    }

    /// Get a cursor pointing to the root element
    pub fn root_nav(&self) -> VtdCursor {
        VtdCursor {
            depth: 0,
            path: vec![],
            doc: self,
        }
    }

    //TODO: SAX (just iterate through records raising events)
}
// immutable cursor
//
// what is needed here?
// [x] move the cursor from element-to-element
// [ ] match the element name/namespace
// [ ] move the cursor based on element name
// [x] has attribute? attribute value
// [ ] index of text content
// [ ] to string?
// [ ] parse as number? (seems unlikely)
//
// higher level cursor
// select elements by name and iterate
// evaluate xpath 1.0
// evaluate xpath 2.0
pub struct VtdCursor<'a> {
    pub depth: u32,
    pub path: Vec<usize>,
    pub doc: &'a ParsedXml,
}
impl<'a> VtdCursor<'a> {
    pub fn root(&self) -> VtdCursor {
        VtdCursor {
            depth: 0,
            path: vec![],
            doc: self.doc,
        }
    }

    pub fn index(&self) -> usize {
        match self.path.len() {
            0 => self.doc.root,
            _ => *self.path.last().unwrap(),
        }
    }

    pub fn raw_text(&self) -> String {
        self.doc.raw_token_val(self.index())
    }

    // TODO different behaviour based on token type
    // for now just do entity resolution and line-ending normalization
    pub fn text(&self) -> String {
        let txt = self.doc.token_val(self.index());
        txt.replace("\r\n", "\n").replace('\r', "\n")
    }

    /// Navigation helper - does the cursor point at an element?
    pub fn is_element(&self) -> bool {
        self.doc.is_element(self.index())
    }

    /// Get the parent element, if it exists
    pub fn parent(&self) -> Option<VtdCursor> {
        if self.path.is_empty() {
            return None;
        }
        if self.path.len() == 1 {
            return Some(self.root());
        }
        let mut n = self.path.clone();
        n.pop();
        Some(VtdCursor {
            depth: self.depth - 1,
            path: n,
            doc: self.doc,
        })
    }

    /// Get the first child element, if it exists
    pub fn first_child(&self) -> Option<VtdCursor> {
        let start = self.index();
        let iter = self.doc.records.iter().enumerate().skip(start + 1);
        for (index, rec) in iter {
            let tt = &rec.kind;
            match tt {
                TokenType::StartTag => {
                    if rec.depth <= self.depth {
                        return None;
                    }
                    let mut n = self.path.clone();
                    n.push(index);
                    return Some(VtdCursor {
                        depth: rec.depth,
                        path: n,
                        doc: self.doc,
                    });
                }
                TokenType::AttrNs | TokenType::AttrName | TokenType::AttrVal => {
                    continue;
                }
                TokenType::CharData | TokenType::Comment | TokenType::CDataVal => {
                    let mut n = self.path.clone();
                    n.push(index);
                    return Some(VtdCursor {
                        depth: rec.depth,
                        path: n,
                        doc: self.doc,
                    });
                }
                _ => {
                    println!("{:?}", rec);
                }
            }
        }
        None
    }
    /// Get the last child (not yet implemented)
    pub fn last_child(&self) -> Option<VtdCursor> {
        None
    }
    /// Get the next sibling element, if it exists
    pub fn next_sibling(&self) -> Option<VtdCursor> {
        let start = self.index();
        let sibling_depth = if self.doc.is_element(start) {
            self.depth
        } else {
            self.depth + 1
        };

        let iter = self.doc.records.iter().enumerate().skip(start + 1);
        for (index, rec) in iter {
            let tt = &rec.kind;
            match tt {
                TokenType::StartTag => {
                    // no next sibling
                    if rec.depth < sibling_depth {
                        return None;
                    }
                    if rec.depth > sibling_depth {
                        continue;
                    }
                    let mut n = self.path.clone();
                    n.pop();
                    n.push(index);
                    return Some(VtdCursor {
                        depth: rec.depth,
                        path: n,
                        doc: self.doc,
                    });
                }
                TokenType::AttrNs | TokenType::AttrName | TokenType::AttrVal => {
                    continue;
                }
                TokenType::CharData | TokenType::Comment | TokenType::CDataVal => {
                    if rec.depth < sibling_depth - 1 {
                        return None;
                    }
                    if rec.depth > sibling_depth - 1 {
                        continue;
                    }
                    let mut n = self.path.clone();
                    n.pop();
                    n.push(index);
                    return Some(VtdCursor {
                        depth: rec.depth,
                        path: n,
                        doc: self.doc,
                    });
                }
                _ => {
                    println!("UNHANDLED {:?}", rec);
                }
            }
        }
        None
    }
    /// Get the previous sibling element (if it exists)
    pub fn prev_sibling(&self) -> Option<VtdCursor> {
        let start = self.index();
        let sibling_depth = if self.doc.is_element(start) {
            self.depth
        } else {
            self.depth + 1
        };
        //slice from parent to start
        let parent = self.parent().map_or(0, |x| x.index());
        //iterate in reverse
        let iter = self.doc.records[parent..start]
            .iter()
            .enumerate()
            .map(|(x, r)| (x + parent, r))
            .rev();
        for (index, rec) in iter {
            //println!("PREV Index {} vs parent {}", index, parent);
            if index == parent {
                return None;
            }
            let tt = &rec.kind;
            match tt {
                TokenType::StartTag => {
                    // no prev sibling
                    if rec.depth == sibling_depth {
                        let mut n = self.path.clone();
                        n.pop();
                        n.push(index);
                        return Some(VtdCursor {
                            depth: rec.depth,
                            path: n,
                            doc: self.doc,
                        });
                    }
                }
                TokenType::AttrNs | TokenType::AttrName | TokenType::AttrVal => {
                    continue;
                }
                TokenType::CharData | TokenType::Comment | TokenType::CDataVal => {
                    if rec.depth == sibling_depth - 1 {
                        let mut n = self.path.clone();
                        n.pop();
                        n.push(index);
                        return Some(VtdCursor {
                            depth: rec.depth,
                            path: n,
                            doc: self.doc,
                        });
                    }
                }
                _ => {
                    println!("UNHANDLED {:?}", rec);
                }
            }
        }
        None
    }

    /// Test whether current element has an attribute with the matching name.
    ///
    /// "*" will match any attribute name, so this can be used to test if there are any attributes
    pub fn has_attr(&self, name: &str) -> bool {
        let start = self.index();
        let iter = self.doc.records.iter().enumerate().skip(start + 1);
        for (index, rec) in iter {
            let tt = &rec.kind;
            match tt {
                TokenType::AttrName => {
                    if name == "*" {
                        return true;
                    }
                    let attr_name = self.doc.raw_token_val(index);
                    if attr_name == name {
                        return true;
                    }
                    continue;
                }
                TokenType::AttrNs | TokenType::AttrVal => {
                    continue;
                }
                _ => {
                    return false;
                }
            }
        }
        false
    }

    pub fn matches_element(&self, name: &str) -> bool {
        let start = self.index();
        let element_name = self.doc.raw_token_val(start);
        name == element_name
    }

    /// Get the attribute's value, if any
    pub fn attr_value(&self, name: &str) -> Option<String> {
        let start = self.index();
        let iter = self.doc.records.iter().enumerate().skip(start + 1);
        for (index, rec) in iter {
            let tt = &rec.kind;
            match tt {
                TokenType::AttrName => {
                    let attr_name = self.doc.raw_token_val(index);
                    if attr_name == name {
                        // this is safe because a valid XML document will always have AttrVal directly after AttrName
                        let val = self.doc.token_val(index + 1);
                        return Some(val);
                    }
                    continue;
                }
                TokenType::AttrVal => {
                    continue;
                }
                TokenType::AttrNs => {
                    continue;
                }
                _ => {
                    return None;
                }
            }
        }
        None
    }

    // lookupNS(self, prefix) -> URL
    //
}
// useful xpath 1 subset
#[cfg(test)]
mod tests {
    //use super::*;
    use crate::parse_doc;

    #[test]
    fn root_first_child() {
        let x = "<a><b /></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let fc = root.first_child().unwrap();
        assert_eq!("b", fc.raw_text());
        assert!(fc.first_child().is_none());
    }

    #[test]
    fn first_child_ignores_attr() {
        let x = "<a x='1' y='2'><b /></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let fc = root.first_child().unwrap();
        assert_eq!("b", fc.raw_text());
        assert!(fc.first_child().is_none());
    }

    #[test]
    fn no_first_child() {
        let x = "<a/>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let fc = root.first_child();
        assert!(fc.is_none());
    }

    #[test]
    fn root_has_no_parent() {
        let x = "<a/>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let p = root.parent();
        assert!(p.is_none());
    }

    #[test]
    fn first_child_to_parent() {
        let x = "<a><b /></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let fc = root.first_child().unwrap();
        let root2 = fc.parent().unwrap();
        assert_eq!(root.depth, root2.depth);
    }

    #[test]
    fn second_child_to_parent() {
        let x = "<a><b /><c /></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let b = root.first_child().unwrap();
        let c = b.next_sibling().unwrap();
        let root2 = c.parent().unwrap();
        assert_eq!(root.depth, root2.depth);
    }

    #[test]
    fn next_sibling_element() {
        let x = "<a><b>foo</b><c>bar</c></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let a = doc.root_nav();
        let b = a.first_child().unwrap();
        assert_eq!("b", b.raw_text());
        let c = b.next_sibling().unwrap();
        assert_eq!("c", c.raw_text());
        assert!(c.next_sibling().is_none());
    }

    #[test]
    fn next_sibling_text() {
        let x = "<a><b>foo</b>bar</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let a = doc.root_nav();
        let b = a.first_child().unwrap();
        assert_eq!("b", b.raw_text());
        let txt = b.next_sibling().unwrap();
        assert_eq!("bar", txt.raw_text());
        assert!(txt.next_sibling().is_none());
    }

    #[test]
    fn next_sibling_after_text() {
        let x = "<a>foo<b>baz</b>bar</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let a = doc.root_nav();
        let foo = a.first_child().unwrap();
        assert_eq!("foo", foo.raw_text());
        let b = foo.next_sibling().unwrap();
        assert_eq!("b", b.raw_text());
        let txt = b.next_sibling().unwrap();
        assert_eq!("bar", txt.raw_text());
        assert!(txt.next_sibling().is_none());
    }
    #[test]
    fn prev_sibling_element() {
        let x = "<a><b>foo</b><c>bar</c></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let a = doc.root_nav();
        let b = a.first_child().unwrap();
        let c = b.next_sibling().unwrap();
        assert_eq!("c", c.raw_text());
        let b2 = c.prev_sibling().unwrap();
        assert_eq!("b", b2.raw_text());
        assert!(b.prev_sibling().is_none());
    }

    #[test]
    fn prev_sibling_text() {
        let x = "<a><b>foo</b>bar</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let a = doc.root_nav();
        let b = a.first_child().unwrap();
        let txt = b.next_sibling().unwrap();
        assert_eq!("bar", txt.raw_text());
        let b2 = txt.prev_sibling().unwrap();
        assert_eq!("b", b2.raw_text());
        assert!(b.prev_sibling().is_none());
    }

    #[test]
    fn prev_sibling_after_text() {
        let x = "<a>foo<b>baz</b>bar</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let a = doc.root_nav();
        let foo = a.first_child().unwrap();
        let b = foo.next_sibling().unwrap();
        let txt = b.next_sibling().unwrap();
        assert_eq!("bar", txt.raw_text());
        let b2 = txt.prev_sibling().unwrap();
        assert_eq!("b", b2.raw_text());
        let foo2 = b2.prev_sibling().unwrap();
        assert_eq!("foo", foo2.raw_text());
        assert!(foo2.prev_sibling().is_none());
    }

    #[test]
    fn predefined_entity_in_text() {
        let x = "<a>Hello &amp; Goodbye</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let txt = root.first_child().unwrap();
        assert_eq!("Hello & Goodbye", txt.text());
    }

    #[test]
    fn numeric_entity_in_text() {
        let x = "<a>Hello &#40;Goodbye&#0041;</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let txt = root.first_child().unwrap();
        assert_eq!("Hello (Goodbye)", txt.text());
    }

    #[test]
    fn hex_entity_in_text() {
        let x = "<a>Hello &#x5B;Goodbye&#x005d;</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let txt = root.first_child().unwrap();
        assert_eq!("Hello [Goodbye]", txt.text());
    }

    #[test]
    fn normalize_line_endings() {
        let x = "<a>Hello\rGoodbye\r\n</a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let txt = root.first_child().unwrap();
        assert_eq!("Hello\nGoodbye\n", txt.text());
    }

    #[test]
    fn has_attr_with_no_attributes() {
        let x = "<a></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert_eq!(false, root.has_attr("*"));
    }

    #[test]
    fn has_attr_matching_wildcard() {
        let x = "<a x='foo'></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert_eq!(true, root.has_attr("*"));
    }
    #[test]
    fn has_attr_matching_name() {
        let x = "<a x='foo'></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert_eq!(true, root.has_attr("x"));
    }
    #[test]
    fn has_attr_matching_name_in_later_position() {
        let x = "<a w='miss' x='foo'></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert_eq!(true, root.has_attr("x"));
    }
    #[test]
    fn has_attr_no_matching_name() {
        let x = "<a y='foo'></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert_eq!(false, root.has_attr("x"));
    }

    #[test]
    fn attr_value_with_no_attributes() {
        let x = "<a></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert!(root.attr_value("x").is_none());
    }
    #[test]
    fn attr_value_matching_name() {
        let x = "<a x='foo'></a>";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        let val = root.attr_value("x");
        assert!(val.is_some());
        assert_eq!("foo", val.unwrap());
    }
    #[test]
    fn match_element_name() {
        let x = "<a />";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert!(root.matches_element("a"));
    }
    #[test]
    fn match_element_name_differs() {
        let x = "<a />";
        let doc = parse_doc(x.as_bytes()).unwrap();
        let root = doc.root_nav();
        assert!(!root.matches_element("b"));
    }
}
