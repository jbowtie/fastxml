//! fastxml is a non-validating XML parser compliant with XML 1.0 (Fifth Edition).
//!
//! It is intended to be a fast, accurate, and safe alternative to using libxml, written from
//! scratch in Rust.
//!
//! It uses the VTD algorithm to build an index of the document, avoiding the memory cost of a DOM
//! representation. This has been extended to account for entity expansion according to the core
//! specification.

mod cursor;
mod decoder;
mod parser;

pub use crate::cursor::ParsedXml;
use crate::parser::parse_doc_impl;
use std::error::Error;
use std::fs;
use std::path::{Path, PathBuf};
use std::rc::Rc;

#[cfg(test)]
use crate::parser::ParserError;

// the packed_struct crate
// lets us derive PrimitiveEnum
#[derive(Debug, PartialEq)]
pub enum TokenType {
    StartTag,
    EndTag,
    AttrName,
    AttrNs,
    AttrVal,
    CharData,
    Comment,
    PiName,
    PiVal,
    DecAttrName,
    DecAttrVal,
    CDataVal,
    DtdVal,
    //Document,
}

// there's a crate called packed_struct that lets us pack the
// record the same way the C implementation does for interop
#[derive(Debug)]
pub struct VtdRecord {
    kind: TokenType,
    depth: u32,
    offset: u64,
    length: u32,
    prefix: Option<u32>,
    source: Option<Rc<[u8]>>,
}

#[derive(Debug)]
pub struct ElementLocation {
    index: usize,
    first_child: Option<usize>,
}

pub enum EntityResolver {
    Disabled, // no resolution
    Local,    // local file system only
    Catalog,  // XML Catalog
    Network,  // full network resolution
}

pub fn parse_file<P: AsRef<Path>>(path: P) -> Result<ParsedXml, Box<dyn Error>> {
    let contents = fs::read(path.as_ref())?;
    let bytes = Rc::<[u8]>::from(contents);

    // we should use some URI crate here and convert paths to URIs
    // then resolve accordingly
    //
    // for initial implementation we just use the containing directory
    // as we are only parsing local test files
    let base = path.as_ref().parent().unwrap();
    let boxed = Box::new(PathBuf::from(base));
    let (records, encoding) = parse_doc_impl(bytes.clone(), Some(boxed))?;
    let root = records.iter().position(|x| x.kind == TokenType::StartTag);
    let output = ParsedXml {
        root: root.unwrap_or(0),
        encoding,
        bytes,
        records,
    };
    Ok(output)
}

// TODO: take Read trait as input as well as byte slice
// use memmap crate for files larger than X (maybe 64KB?)

pub fn parse_doc(bytes: &[u8]) -> Result<ParsedXml, Box<dyn Error>> {
    //parser should detect the encoding and return it
    let b = Rc::<[u8]>::from(bytes);
    let (records, encoding) = parse_doc_impl(b.clone(), None)?;
    let root = records.iter().position(|x| x.kind == TokenType::StartTag);
    let output = ParsedXml {
        root: root.unwrap_or(0),
        encoding,
        bytes: b,
        records,
    };
    Ok(output)
}

/*
 * just sketching out some ideas about the preferred interface
 * we really need to process external entities and DTD when validating
 * internal DTD needs to be checked against well-formed constraints
 * even when not validating
 *
fn parse_something() {
    // catalog - null (no external resolutions)
    // catalog - local (only local filesystem)
    // catalog - network (arbitrary?)
    let parser = XmlParser::parse_xml_file("file.xml")?;
    parser.validate(EntityResolver::Local);

    var p = Parser::from_file(x).use_resolver(local).use_base(path);
    p.parse();
    var e = EntityParser::from_file(x);
    var d = DtdParser::from_file(x); // <== text decl, dtd markup
}
*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_doc_record() {
        let s = "<foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        println!("{:?}", parsed.records);
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    #[should_panic(expected = "Not enough bytes")]
    fn parse_notxml_too_short() {
        let s = "foo";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "XML not starting correctly")]
    fn parse_notxml() {
        let s = "foobar";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "EOF")]
    fn parse_incomplete_doc() {
        let s = "<foo";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }
    #[test]
    fn parse_doc_extra_ws() {
        let s = "<foo /> \r\n ";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    #[should_panic(expected = "Invalid XML declaration")]
    fn parse_missing_version() {
        let s = "<?xml?>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "unexpected version")]
    fn parse_unexpected_version() {
        let s = "<?xml version='2.0'?>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_valid_standalone() {
        let s = "<?xml version='1.0' standalone = 'yes' ?><a/>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "standalone must be")]
    fn parse_invalid_standalone() {
        let s = "<?xml version='1.0' standalone = 'true' ?><a/>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "not terminated properly")]
    fn parse_standalone_wrong_spelling() {
        let s = "<?xml version='1.0' stadnalone='yes' ?><a/>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "Invalid character")]
    fn parse_text_before_root() {
        let s = "<?xml version='1.0'?> bad <root/>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "XML declaration must have version as first attribute")]
    fn parse_version_wrong_location() {
        let s = "<?xml encoding='utf-8' version='1.0'?>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "declaration not terminated properly")]
    fn parse_improperly_terminated_decl() {
        let s = "<?xml version='1.0'?]\n<foo/>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "closing quote")]
    fn parse_mismatched_quote_in_decl() {
        let s = "<?xml version='1.0\"?>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_comment() {
        let s = "<!-- comment --><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(2, parsed.records.len());
        assert_eq!(TokenType::Comment, parsed.records[0].kind);
        assert_eq!(" comment ", parsed.token_val(0));
        assert_eq!(TokenType::StartTag, parsed.records[1].kind);
    }

    #[test]
    #[should_panic(expected = "prohibited sequence in comment")]
    fn parse_illegal_comment() {
        let s = "<!-- comment -- not allowed --><foo />";
        let _parsed = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    #[should_panic(expected = "EOF")]
    fn parse_unclosed_comment() {
        let s = "<!-- bad syntax><foo />";
        let _parsed = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_end_comment() {
        let s = "<foo />\n<!--a-->\n<!-- b -->";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(3, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!(TokenType::Comment, parsed.records[1].kind);
        assert_eq!(TokenType::Comment, parsed.records[2].kind);
        assert_eq!("a", parsed.token_val(1));
        assert_eq!(" b ", parsed.token_val(2));
    }

    #[test]
    fn parse_cdata() {
        let s = "<a><![CDATA[<b>]]></a>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(2, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!(TokenType::CDataVal, parsed.records[1].kind);
        assert_eq!("<b>", parsed.token_val(1));
    }

    #[test]
    fn parse_short_pi() {
        let s = "<a><?foo?></a>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(3, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!(TokenType::PiName, parsed.records[1].kind);
        assert_eq!(TokenType::PiVal, parsed.records[2].kind);
        assert_eq!("foo", parsed.token_val(1));
        assert_eq!("", parsed.token_val(2));
    }

    #[test]
    fn parse_long_pi() {
        let s = "<a><?some-pi arg list?></a>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(3, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!(TokenType::PiName, parsed.records[1].kind);
        assert_eq!(TokenType::PiVal, parsed.records[2].kind);
        assert_eq!("some-pi", parsed.token_val(1));
        assert_eq!("arg list", parsed.token_val(2));
    }

    #[test]
    fn parse_end_pi() {
        let s = "<foo />\n<?pi data?>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(3, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!(TokenType::PiName, parsed.records[1].kind);
        assert_eq!(TokenType::PiVal, parsed.records[2].kind);
        assert_eq!("pi", parsed.token_val(1));
        assert_eq!("data", parsed.token_val(2));
    }

    #[test]
    #[should_panic(expected = "invalid CDATA section close")]
    fn parse_invalid_text() {
        let s = "<a>Hello ]]> world</a>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_predefined_entity() {
        let s = "<a>hello &amp; world</a>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(2, parsed.records.len());
        assert_eq!(TokenType::CharData, parsed.records[1].kind);
    }

    #[test]
    fn parse_numeric_entity() {
        let s = "<a>hello &#39; &#x201C;world</a>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(2, parsed.records.len());
        assert_eq!(TokenType::CharData, parsed.records[1].kind);
    }

    #[test]
    fn parse_bad_numeric_entity() {
        let s = "<a>hello &#201C; world</a>";
        let result = parse_doc(s.as_bytes()).err().unwrap();
        let expected = ParserError::InvalidNumericEntity;
        assert_eq!(expected.to_string(), result.to_string());
    }

    #[test]
    fn parse_bad_hex_entity() {
        let s = "<a>hello &#x201G; world</a>";
        let result = parse_doc(s.as_bytes()).err().unwrap();
        let expected = ParserError::InvalidHexEntity;
        assert_eq!(expected.to_string(), result.to_string());
    }

    #[test]
    fn parse_unknown_entity() {
        let s = "<a>hello &nbsp; world</a>";
        let result = parse_doc(s.as_bytes()).unwrap_err();
        let expected = ParserError::UnknownEntity(1, "nbsp".to_string());
        assert_eq!(expected.to_string(), result.to_string());
    }

    #[test]
    fn parse_predefined_entity_in_attr() {
        let s = "<a char='&quot;'>hello world</a>";
        let _parsed = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_unknown_entity_in_attr() {
        let s = "<a char='&nbsp;'>hello world</a>";
        let result = parse_doc(s.as_bytes()).unwrap_err();
        let expected = ParserError::UnknownEntity(1, "nbsp".to_string());
        assert_eq!(expected.to_string(), result.to_string());
    }
    #[test]
    fn parse_mismatched_end_tag() {
        let s = "<foo></bar>";
        let e = parse_doc(s.as_bytes()).unwrap_err();
        assert_eq!(e.to_string(), "End tag does not match start tag");
    }
    #[test]
    fn parse_inproperly_nested_tag() {
        let s = "<foo><b><i></b></i></foo>";
        let e = parse_doc(s.as_bytes()).unwrap_err();
        assert_eq!(e.to_string(), "End tag does not match start tag");
    }
    #[test]
    #[should_panic(expected = "EOF")]
    fn parse_missing_end_tag() {
        let s = "<foo><a/>";
        let _records = parse_doc(s.as_bytes()).unwrap();
    }
    #[test]
    fn parse_malformed_end_tag() {
        let s = "<foo></foox>";
        let e = parse_doc(s.as_bytes()).unwrap_err();
        assert_eq!(e.to_string(), "End tag does not match start tag");
    }

    #[test]
    fn parse_specify_utf8() {
        let s = "<?xml version='1.0' encoding='utf-8'?><foo/>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!("UTF-8", parsed.encoding.name());
    }

    #[test]
    fn parse_with_bom() {
        let s = b"\xEF\xBB\xBF<foo />";
        let parsed = parse_doc(s).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_utf16be_with_bom() {
        let s = b"\xFE\xFF\0<\0f\0o\0o\0 \0/\0>";
        let parsed = parse_doc(s).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }
    #[test]
    fn parse_utf16le_with_bom() {
        let s = b"\xFF\xFE<\0f\0o\0o\0 \0/\0>\0";
        let parsed = parse_doc(s).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }
    #[test]
    fn parse_utf16be_with_surrogate_pair() {
        let s = b"\xFE\xFF\0<\0f\0o\xD8\x01\xDC\x37\0o\0 \0/\0>";
        let parsed = parse_doc(s).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("fo\u{10437}o", parsed.token_val(0));
    }

    #[test]
    fn parse_specify_8859_1() {
        let s = "<?xml version='1.0' encoding='iso-8859-1'?><foo/>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        // per the Encoding Standard - https://encoding.spec.whatwg.org/
        assert_eq!("windows-1252", parsed.encoding.name());
    }

    #[test]
    fn parse_specify_ascii() {
        let s = "<?xml version='1.0' encoding='US-ASCII'?><foo/>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        // per the Encoding Standard - https://encoding.spec.whatwg.org/
        assert_eq!("windows-1252", parsed.encoding.name());
    }
    #[test]
    fn parse_specify_8859_2() {
        let s = "<?xml version='1.0' encoding='iso-8859-2'?><foo/>";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!("ISO-8859-2", parsed.encoding.name());
    }

    #[test]
    fn parse_minimal_doctype() {
        let s = "<!DOCTYPE foo><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_system_doctype() {
        let s = "<!DOCTYPE foo SYSTEM 'http://example.com'><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_system_doctype_follow_whitespace() {
        let s = "<!DOCTYPE foo SYSTEM 'example.dtd'>\n<foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    #[should_panic(expected = "Invalid character")]
    fn parse_system_doctype_bad_following_char() {
        let s = "<!DOCTYPE foo SYSTEM 'http://example.com'>x<foo />";
        let _parsed = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_system_doctype_with_internal() {
        let s = "<!DOCTYPE foo SYSTEM 'http://example.com'[]><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_doctype_with_internal() {
        let s = "<!DOCTYPE foo []><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_doctype_with_internal_no_space() {
        let s = "<!DOCTYPE foo[]><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_public_doctype() {
        let s = "<!DOCTYPE foo PUBLIC 'public id' 'http://example.com'><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_public_doctype_with_internal() {
        let s = "<!DOCTYPE foo PUBLIC 'id' 'http://example.com'[]><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }

    #[test]
    fn parse_public_doctype_missing_systemid() {
        let s = "<!DOCTYPE foo PUBLIC 'public id' ><foo />";
        let parsed = parse_doc(s.as_bytes()).expect_err("Should complain");
        assert!(parsed
            .to_string()
            .contains("expected identifier enclosed in quotes"));
    }

    #[test]
    #[should_panic(expected = "Invalid character")]
    fn parse_public_doctype_bad_following_char() {
        let s = "<!DOCTYPE foo PUBLIC 'id' 'http://example.com'>x<foo />";
        let _parsed = parse_doc(s.as_bytes()).unwrap();
    }

    #[test]
    fn parse_public_doctype_with_internal_entity() {
        let s = "<!DOCTYPE foo [<!ENTITY x '1'>]><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }
    #[test]
    fn parse_public_doctype_with_internal_system_entity() {
        let s = "<!DOCTYPE foo [<!ENTITY x SYSTEM '1'>]><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }
    #[test]
    fn parse_public_doctype_with_internal_public_entity() {
        let s = "<!DOCTYPE foo [<!ENTITY x PUBLIC '1' '2'>]><foo />";
        let parsed = parse_doc(s.as_bytes()).unwrap();
        assert_eq!(1, parsed.records.len());
        assert_eq!(TokenType::StartTag, parsed.records[0].kind);
        assert_eq!("foo", parsed.token_val(0));
    }
    #[test]
    fn parse_public_doctype_with_bad_public_entity() {
        let s = "<!DOCTYPE foo [<!ENTITY x PUBLIK '1' '2'>]><foo />";
        let parsed = parse_doc(s.as_bytes()).expect_err("Should complain");
        assert!(parsed.to_string().contains("PUBLIC"));
    }
}
