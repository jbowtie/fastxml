extern crate fastxml;

use std::fs;
use std::path::{Path, PathBuf};

// Sun Microsystems XML Tests

#[test]
fn sun_not_wf_not_sa03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/not-sa03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 11: unknown entity \'number\'");
}

#[test]
fn sun_not_wf_attlist01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn sun_not_wf_attlist02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn sun_not_wf_attlist03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: character \',\' not allowed in attribute definition"
    );
}

#[test]
fn sun_not_wf_attlist04() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn sun_not_wf_attlist05() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn sun_not_wf_attlist06() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn sun_not_wf_attlist07() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 7: expected \'NOTATION\' at this location"
    );
}

#[test]
fn sun_not_wf_attlist08() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn sun_not_wf_attlist09() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid attribute default in definition");
}

#[test]
fn sun_not_wf_attlist10() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
fn sun_not_wf_attlist11() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/attlist11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Whitespace required between attribute/value pairs"
    );
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn sun_not_wf_cond01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/cond01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn sun_not_wf_cond02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/cond02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn sun_not_wf_content01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/content01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn sun_not_wf_content02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/content02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn sun_not_wf_content03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/content03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn sun_not_wf_decl01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/decl01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 6: unknown entity \'ent01\'");
}

#[test]
#[ignore = "INVESTIGATE"]
fn sun_not_wf_dtd00() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd00.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in internal subset declaration"
    );
}

#[test]
fn sun_not_wf_dtd01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Cannot mix connectors in element content sequence"
    );
}

#[test]
fn sun_not_wf_dtd02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 5: character \' \' not allowed  in entity name"
    );
}

#[test]
fn sun_not_wf_dtd03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 5: character \'\n\' not allowed  in entity name"
    );
}

#[test]
fn sun_not_wf_dtd04() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
fn sun_not_wf_dtd05() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "expected identifier enclosed in quotes");
}

#[test]
#[ignore = "Need to support external DTD resolution to trigger the problem"]
fn sun_not_wf_dtd07() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/dtd07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Text declarations (which optionally begin any external entity) are required to have encoding");
}

#[test]
fn sun_not_wf_element00() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/element00.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn sun_not_wf_element01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/element01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn sun_not_wf_element02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/element02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
fn sun_not_wf_element03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/element03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Invalid character"));
}

#[test]
fn sun_not_wf_element04() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/element04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Unrecognized sequence"));
}

#[test]
fn sun_not_wf_encoding01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "XML declaration: whitespace not allowed in encoding name"
    );
}

#[test]
fn sun_not_wf_encoding02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn sun_not_wf_encoding03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn sun_not_wf_encoding04() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn sun_not_wf_encoding05() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn sun_not_wf_encoding06() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("unknown encoding"));
}

#[test]
fn sun_not_wf_encoding07() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/encoding07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 10: unknown entity \'empty\'");
}

#[test]
fn sun_not_wf_pi() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/pi.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: missing required whitespace");
}

#[test]
fn sun_not_wf_pubid01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/pubid01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn sun_not_wf_pubid02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/pubid02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn sun_not_wf_pubid03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/pubid03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn sun_not_wf_pubid04() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/pubid04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Invalid character found in PUBLIC id");
}

#[test]
fn sun_not_wf_pubid05() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/pubid05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 5: missing required whitespace");
}

#[test]
fn sun_not_wf_sgml01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Unexpected EOF");
}

#[test]
fn sun_not_wf_sgml02() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml02.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "PI tags cannot be named \'xml\' (case-insensitive)"
    );
}

#[test]
fn sun_not_wf_sgml03() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml03.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert!(e.to_string().contains("Comment improperly terminated"));
}

#[test]
fn sun_not_wf_sgml04() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml04.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "ATTLIST name is required");
}

#[test]
fn sun_not_wf_sgml05() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml05.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 8: missing required whitespace");
}

#[test]
fn sun_not_wf_sgml06() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml06.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "ATTLIST name is required");
}

#[test]
fn sun_not_wf_sgml07() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml07.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn sun_not_wf_sgml08() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml08.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn sun_not_wf_sgml09() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml09.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 4: expected \'>\' at this location");
}

#[test]
fn sun_not_wf_sgml10() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml10.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(e.to_string(), "Line 3: expected \'>\' at this location");
}

#[test]
fn sun_not_wf_sgml11() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml11.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn sun_not_wf_sgml12() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml12.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Expected content model in element declaration"
    );
}

#[test]
fn sun_not_wf_sgml13() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/sgml13.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Invalid character in element content declaration"
    );
}

#[test]
fn sun_not_wf_uri01() {
    let contents = fs::read("./tests/xmlconf/sun/not-wf/uri01.xml").unwrap();
    let e = fastxml::parse_doc(&contents).unwrap_err();
    assert_eq!(
        e.to_string(),
        "Line 4: character \'#\' not allowed in SYSTEM identifier"
    );
}

// valid

#[test]
fn sun_valid_pe01() {
    let contents = fs::read("./tests/xmlconf/sun/valid/pe01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn sun_valid_dtd00() {
    let contents = fs::read("./tests/xmlconf/sun/valid/dtd00.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn sun_valid_dtd01() {
    let contents = fs::read("./tests/xmlconf/sun/valid/dtd01.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn sun_valid_element() {
    let parsed = fastxml::parse_file("./tests/xmlconf/sun/valid/element.xml").unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn sun_valid_ext01() {
    let parsed = fastxml::parse_file("./tests/xmlconf/sun/valid/ext01.xml").unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn check_relative_url() {
    let doc = Path::new("./tests/xmlconf/sun/valid/ext01.xml");
    let ent = doc.parent().unwrap().join("ext01.ent");
    assert_eq!(ent, PathBuf::from("./tests/xmlconf/sun/valid/ext01.ent"));
}

#[test]
#[ignore = "offset error in external with XmlDecoder"]
fn sun_valid_ext02() {
    let parsed = fastxml::parse_file("./tests/xmlconf/sun/valid/ext02.xml").unwrap();
    assert_eq!("foo", parsed.token_val(parsed.root));
}

#[test]
fn sun_valid_not_sa01() {
    let parsed = fastxml::parse_file("./tests/xmlconf/sun/valid/not-sa01.xml").unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}

#[test]
fn sun_valid_not_sa02() {
    let contents = fs::read("./tests/xmlconf/sun/valid/not-sa02.xml").unwrap();
    let parsed = fastxml::parse_doc(&contents).unwrap();
    assert_eq!("attributes", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "external DTD"]
fn sun_valid_not_sa03() {
    let parsed = fastxml::parse_file("./tests/xmlconf/sun/valid/not-sa03.xml").unwrap();
    assert_eq!("attributes", parsed.token_val(parsed.root));
}

#[test]
#[ignore = "external DTD"]
fn sun_valid_not_sa04() {
    let parsed = fastxml::parse_file("./tests/xmlconf/sun/valid/not-sa03.xml").unwrap();
    assert_eq!("root", parsed.token_val(parsed.root));
}
