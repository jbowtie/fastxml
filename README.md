This is a naive re-implementation of the XML-VTD algorithm in Rust.

The long-term goal for this library is to provide a Rust-native alternative to libxml.

The short-term goal is to pass the XML conformance test suite.

* Most documents that are not well-formed are rejected.
* Most valid documents are passed.

The important features that are still missing for conformance:

* Entity expansion needs to happen
* There should be an option to parse external DTDs

Adding validation should be behind a feature flag.

* We can claim conformance as a non-validating parser by skipping the 'invalid' tests and/or passing such documents

Part one:
* [ ] Reject all standalone documents that are not well-formed
* [ ] Parse all well-formed standalone documents

Part two:
* [ ] Handle non-standalone documents

Part three:
* [ ] Recognize if document is valid or invalid

Part four:
* [ ] Serialize in canonical format

## TODO

* [x] Parse comments
* [x] Parse CDATA, PI
* [x] Parse built-in entities
* [x] Have immutable cursors into the document.
* [x] Expand test suite to cover various error sequences.
* [x] Recognize and handle encodings required by XML.
* [x] Handle common encodings beyond the most basic.
* [ ] Handle expansion of entities declared in DTD.
* [ ] Handle external entities/DTDs.
* [ ] Do XML validation.
* [ ] Improve the quality of our error messages.
* [ ] Start benchmarking against C implementation.
* [ ] Add XPath support (XPath 1.x)
* [ ] Add XQuery support (basis of XPath 2.x).
* [ ] Have mutable cursor (unclear if actually needed)
* [ ] Add SAX API support
* [ ] Add DOM API support
