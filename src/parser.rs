use crate::decoder::{DecodedChar, XmlDecoder};
use crate::TokenType;
use crate::VtdRecord;
use encoding_rs::Encoding;
use encoding_rs::{UTF_16BE, UTF_16LE, UTF_8};
use std::collections::HashMap;
use std::fs;
use std::io::BufReader;
use std::io::Cursor;
use std::path::PathBuf;
use std::rc::Rc;
use std::str;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ParserError {
    #[error("Unexpected EOF")]
    EOF,
    #[error("invalid numeric entity")]
    InvalidNumericEntity,
    #[error("invalid hex entity")]
    InvalidHexEntity,
    #[error("invalid character")]
    InvalidCharacter,
    #[error("duplicate attribute")]
    DuplicateAttribute,
    #[error("Line {0}: character '{1}' not allowed {2}")]
    CharacterNotAllowed(u32, char, String),
    #[error("Line {0}: missing required whitespace")]
    RequiredWhitespace(u32),
    #[error("Line {0}: expected '{1}' at this location")]
    Expected(u32, String),
    #[error("End tag does not match start tag")]
    MismatchedTag,
    #[error("Line {0}: unknown entity '{1}'")]
    UnknownEntity(u32, String),
    #[error("Line {0}: The external reference '{1}' could not be resolved")]
    UnresolvedExternal(u32, String),
    #[error("Line {0}: unknown encoding '{1}'")]
    UnknownEncoding(u32, String),
    #[error("{0}")]
    Other(String),
}

fn is_xml_whitespace(c: char) -> bool {
    " \n\r\t".chars().any(|x| x == c)
}

fn is_valid_char(c: char) -> bool {
    // this is the XML 1.0 definition
    matches!(c, '\n' | '\r' | '\t' | '\u{20}'..='\u{D7FF}' | '\u{E000}'..='\u{FFFD}' | '\u{10000}'..='\u{10FFFF}')

    // the XML 1.1 definition allows but restricts:
    // U+0001–U+0008, U+000B–U+000C, U+000E–U+001F, U+007F–U+0084, U+0086–U+009F
    //
    // allowed but discouraged
    // U+FDD0–U+FDEF, U+1FFFE–U+1FFFF, U+2FFFE–U+2FFFF, U+3FFFE–U+3FFFF, U+4FFFE–U+4FFFF,
    // U+5FFFE–U+5FFFF, U+6FFFE–U+6FFFF, U+7FFFE–U+7FFFF, U+8FFFE–U+8FFFF, U+9FFFE–U+9FFFF,
    // U+AFFFE–U+AFFFF, U+BFFFE–U+BFFFF, U+CFFFE–U+CFFFF, U+DFFFE–U+DFFFF, U+EFFFE–U+EFFFF,
    // U+FFFFE–U+FFFFF, U+10FFFE–U+10FFFF
}

// See https://www.w3.org/TR/REC-xml/#NT-NameStartChar for definition
fn is_namestart_char(c: char) -> bool {
    matches!(c, ':' | '_' | 'A'..='Z' | 'a'..='z' | '\u{C0}'..='\u{D6}' | '\u{D8}'..='\u{F6}' | '\u{F8}'..='\u{2FF}' | '\u{370}'..='\u{37D}' | '\u{37F}'..='\u{1FFF}' | '\u{200C}'..='\u{200D}' | '\u{2070}'..='\u{218F}' | '\u{2C00}'..='\u{2FEF}' | '\u{3001}'..='\u{D7FF}' | '\u{F900}'..='\u{FDCF}' | '\u{FDF0}'..='\u{FFFD}' | '\u{10000}'..='\u{EFFFF}')
}

// See https://www.w3.org/TR/REC-xml/#NT-NameChar for definition
fn is_name_char(c: char) -> bool {
    if is_namestart_char(c) {
        return true;
    }
    matches!(c, '-' | '.' | '0'..='9' | '\u{B7}' | '\u{0300}'..='\u{036F}' | '\u{203F}'..='\u{2040}')
}

// 	See https://www.w3.org/TR/REC-xml/#NT-PubidLiteral for definition
fn is_pubid_literal_char(c: char) -> bool {
    matches!(c, ' ' | '\n' | '\r' | '-' | '\'' | '(' | ')' | '+' | ',' | '.' | '/' | ':' | '=' | '?' | ';' | '!' | '*' | '#' | '@' | '$' | '_' | '%' | 'A'..='Z' | 'a'..='z' | '0'..='9')
}

#[derive(Debug, PartialEq)]
enum ParseState {
    DocStart,
    DocEnd,
    LtSeen,
    DecAttrName,
    StartTag,
    EndTag,
    PiTag,
    PiVal,
    Comment,
    AttrName,
    AttrVal,
    Text,
    //Entity,
    CData,
    EndComment, // comment at end of document
    EndPi,      //PI at end of document
    Complete,
    DtdMarkup,
    ExternalStart,
    TextDecl,
}

// line and charpos are strictly for error reporting
// is there a trait or rust idiom for this?
//
// bytes is the raw data, needed by VtdRecords
// entity_decl is used to resolve custom entities
//
// xml_base is used to resolve relative paths
// such as used by system identifiers
// will want resolution to go through catalog eventually
// see also url crate
struct Parser {
    bytes: Rc<[u8]>,
    encoder: &'static Encoding,
    reader: XmlDecoder<BufReader<Cursor<Rc<[u8]>>>>,
    offset: usize,
    depth: i32,
    state: ParseState,
    records: Vec<VtdRecord>,
    tag_stack: Vec<VtdRecord>,
    entity_decl: HashMap<String, String>,
    xml_base: Option<Box<PathBuf>>,
    line: u32,
    charpos: u32,
    seen: Option<DecodedChar>,
    ignore_doctype: bool,
}

#[derive(Debug, PartialEq)]
enum EntityContext {
    Content,
    Attr,
    Entity,
}

/*
#[derive(Debug, PartialEq)]
enum EntityValue {
    Raw(String),
    UnresolvedExternal,
}
*/

impl Parser {
    /// Use this for parsing an XML file.
    fn new(bytes: Rc<[u8]>, encoder: &'static Encoding) -> Parser {
        let c = Cursor::new(bytes.clone());
        let buf = BufReader::new(c);
        let decoder = XmlDecoder::new(buf, encoder);
        //let it = Box::new(decoder.peekable()) as Box<dyn Iterator<Item=DecodedChar>>;
        Parser {
            bytes,
            encoder,
            reader: decoder,
            offset: 0,
            depth: -1,
            state: ParseState::DocStart,
            records: Vec::new(),
            tag_stack: Vec::new(),
            entity_decl: HashMap::new(),
            xml_base: None,
            line: 1,
            charpos: 0,
            seen: None,
            ignore_doctype: false,
        }
    }

    /// Use this for parsing an entity, DTD, or other external reference
    fn from_source(bytes: &[u8], encoder: &'static Encoding, state: ParseState) -> Parser {
        let bb: Rc<[u8]> = bytes.into();
        let c = Cursor::new(bb.clone());
        let buf = BufReader::new(c);
        let decoder = XmlDecoder::new(buf, encoder);
        Parser {
            bytes: bb,
            encoder,
            reader: decoder,
            offset: 0,
            depth: -1,
            state,
            records: Vec::new(),
            tag_stack: Vec::new(),
            entity_decl: HashMap::new(),
            xml_base: None,
            line: 1,
            charpos: 0,
            seen: None,
            ignore_doctype: false,
        }
    }

    fn ignore_dtd(&mut self) {
        self.ignore_doctype = true;
    }

    /// Returns the next character and its width in the parser's buffer (but does not change the offset)
    fn peek_opt(&mut self) -> (Option<char>, usize) {
        if let Some(d) = self.seen.as_ref() {
            //if d.0 == '-' {println!("using seen {} offset {}", d.0, self.offset);}
            return (Some(d.0), d.1);
        }

        let z = self.reader.next();
        if z.is_none() {
            self.seen = None;
            return (None, 0);
        }

        self.seen = z;
        if let Some(d) = self.seen.as_ref() {
            return (Some(d.0), d.1);
        }
        (None, 0)
    }

    /// Peek at the next character in the buffer. Returns the `EOF` error if there is no next
    /// character.
    fn peek(&mut self) -> Result<(char, usize), ParserError> {
        let (c, w) = self.peek_opt();
        if c.is_none() {
            return Err(ParserError::EOF);
        }
        let x = c.unwrap();
        //let val = self.read_as_string(self.offset, w);
        //println!("PEEK {}, {} at offset {} really {}", x, w, self.offset, val);
        if is_valid_char(x) {
            return Ok((x, w));
        }
        Err(ParserError::InvalidCharacter)
    }

    /// Advance the parser offset.
    ///
    /// #Remarks
    ///
    /// This takes the character so we can update line and position.
    fn advance(&mut self, val: char, width: usize) {
        if val == '\n' {
            self.line += 1;
            self.charpos = 0;
        } else {
            self.charpos += 1;
        }
        self.offset += width;
        self.seen = None;
    }

    /// Returns the next character in the parser's buffer, advancing the cursor.
    ///
    /// If you don't want to advance the cursor, use `peek` instead.
    fn next(&mut self) -> Result<char, ParserError> {
        let (b, w) = self.peek()?;
        self.advance(b, w);
        Ok(b)
    }

    /// Comsume the next character IF it matches.
    fn skip_if(&mut self, val: char) -> bool {
        let (x, w) = self.peek_opt();
        if x == Some(val) {
            self.advance(val, w);
            return true;
        }
        false
    }

    /// Keep consuming characters until we reach a non-whitespace character
    fn consume_whitespace(&mut self) {
        loop {
            let c = self.peek();
            match c {
                Err(_) => {
                    break;
                }
                Ok((x, w)) => {
                    if !is_xml_whitespace(x) {
                        break;
                    }
                    self.advance(x, w);
                }
            }
        }
    }

    fn consume_required_whitespace(&mut self) -> Result<(), ParserError> {
        let (c, _) = self.peek()?;
        if !is_xml_whitespace(c) {
            return Err(ParserError::RequiredWhitespace(self.line));
        }
        self.consume_whitespace();
        Ok(())
    }

    /// Skip the string if the whole string matches
    ///
    /// Otherwise, the cursor stays where it is
    fn skip_if_matched(&mut self, val: &str) -> bool {
        let temp_offset = self.offset;
        let mut seek = false;
        for c in val.chars() {
            if !self.skip_if(c) {
                if seek {
                    self.reset_pos(temp_offset);
                }
                return false;
            }
            seek = true;
        }
        true
    }

    fn reset_pos(&mut self, pos: usize) {
        //println!("reset to {}", pos);
        self.seen = None;
        self.offset = pos;
        self.reader.seek(pos);
    }

    // Skip the string if it matches, throw an error if it doesn't
    fn expect_match(&mut self, val: &str) -> Result<(), ParserError> {
        if self.skip_if_matched(val) {
            return Ok(());
        }
        Err(ParserError::Expected(self.line, val.to_string()))
    }

    // require a Name token be present.
    // On success, return the start offset and the length
    fn require_name(&mut self) -> Result<(usize, usize), ParserError> {
        let start = self.offset;
        let c = self.next()?;
        if !is_namestart_char(c) {
            return Err(ParserError::Other("invalid name token".to_string()));
        }
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        if length == 0 {
            return Err(ParserError::Other("Name token required here".to_string()));
        }
        Ok((start, length))
    }

    // this should be avoided in favour of building VTD records
    // however there are a couple of places where we want to decode at parse time
    // notably when constructing entity replacement text
    // and when resolving external references
    fn read_as_string(&self, start: usize, length: usize) -> String {
        let (s, _, _) = self.encoder.decode(&self.bytes[start..(start + length)]);
        s.to_string()
    }

    fn resolve_system_id(&self, path: String) -> Result<PathBuf, ParserError> {
        if let Some(p) = &self.xml_base {
            let resolved = p.join(path);
            Ok(resolved)
        } else {
            Err(ParserError::UnresolvedExternal(self.line, path))
        }
    }

    fn process_external_entity(&self, path: PathBuf) -> Result<String, ParserError> {
        // if not validating, we can choose not to resolve, just raise an event
        //println!("Resolving {:?}", path);
        let contents = fs::read(path.as_path()).unwrap();
        let bytes = Rc::<[u8]>::from(contents);
        //let base = path.as_ref().parent().unwrap();
        //let boxed = Box::new(PathBuf::from(base));
        let (_records, _encoding) = parse_ext_entity_impl(bytes, None)?;
        Ok(String::from(path.to_str().unwrap()))
    }

    fn process_start_doc(&mut self) -> Result<ParseState, ParserError> {
        let c = self.next()?;
        if c == '<' {
            let start = self.offset;
            // starting with an XML declaration ('<?xml ...')
            if self.skip_if_matched("?xml") {
                let (d, _) = self.peek()?;
                if is_xml_whitespace(d) {
                    self.consume_whitespace();
                    return Ok(ParseState::DecAttrName);
                }
                // invalid '<?xml?>' (must at least have version attribute)
                if d == '?' {
                    return Err(ParserError::Other("Invalid XML declaration".to_string()));
                }
                // could have initial PI with a target like 'xmlfoo'
                // so fallthrough to PI logic
                self.reset_pos(start);
            }
            return Ok(ParseState::LtSeen);
        }
        //whitespace followed by valid start character
        if is_xml_whitespace(c) {
            self.consume_whitespace();
            if self.next()? == '<' {
                return Ok(ParseState::LtSeen);
            }
        }
        Err(ParserError::Other("XML not starting correctly".to_string()))
    }

    // XML declaration attributes
    fn process_decl_attr(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        if !self.skip_if_matched("version") {
            return Err(ParserError::Other(
                "XML declaration must have version as first attribute".to_string(),
            ));
        }
        self.consume_whitespace();
        let c = self.next()?;
        if c != '=' {
            return Err(ParserError::Other(
                "XML declaration: unexpected character (expected '=')".to_string(),
            ));
        }
        let version = VtdRecord {
            kind: TokenType::DecAttrName,
            depth: 0,
            offset: start as u64,
            length: 7,
            prefix: None,
            source: None,
        };
        self.records.push(version);
        self.consume_whitespace();
        let quote = self.next()?;
        let ver_start = self.offset;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "XML declaration: unexpected character (expected opening quote)".to_string(),
            ));
        }
        if self.skip_if_matched("1.0") {
            let version_attr = VtdRecord {
                kind: TokenType::DecAttrVal,
                depth: 0,
                offset: ver_start as u64,
                length: 3,
                prefix: None,
                source: None,
            };
            self.records.push(version_attr);
        } else {
            return Err(ParserError::Other(
                "XML declaration: unexpected version".to_string(),
            ));
        }
        if self.next()? != quote {
            return Err(ParserError::Other(
                "XML declaration: invalid version closing quote".to_string(),
            ));
        }
        let (c, _) = self.peek()?;
        if !is_xml_whitespace(c) && c != '?' {
            return Err(ParserError::Other(
                "XML declaration: expected whitespace between attributes".to_owned(),
            ));
        }
        self.consume_whitespace();
        // encoding (optional)
        let enc_start = self.offset;
        if self.skip_if_matched("encoding") {
            self.consume_whitespace();
            let c = self.next()?;
            if c != '=' {
                return Err(ParserError::Other(
                    "XML declaration: unexpected character (expected '=')".to_string(),
                ));
            }
            // save record
            let enc = VtdRecord {
                kind: TokenType::DecAttrName,
                depth: 0,
                offset: enc_start as u64,
                length: 10,
                prefix: None,
                source: None,
            };
            self.records.push(enc);
            let quote = self.next()?;
            let val_start = self.offset;
            if quote != '\'' && quote != '"' {
                return Err(ParserError::Other(
                    "XML declaration: unexpected character (expected opening quote)".to_string(),
                ));
            }
            // collect valid characters; break on quote
            let length = loop {
                let (c, w) = self.peek()?;
                // check that is valid
                if is_xml_whitespace(c) {
                    return Err(ParserError::Other(
                        "XML declaration: whitespace not allowed in encoding name".to_string(),
                    ));
                }
                if c == quote {
                    break self.offset - val_start;
                }
                self.advance(c, w);
            };
            // write val record
            let tag = VtdRecord {
                kind: TokenType::DecAttrVal,
                depth: 0,
                offset: val_start as u64,
                length: length as u32,
                prefix: None,
                source: None,
            };
            self.records.push(tag);
            if self.next()? != quote {
                return Err(ParserError::Other(
                    "XML declaration: invalid closing quote".to_string(),
                ));
            }
            // switch the encoding to the specified value
            let (encoding_name, _, _) = self
                .encoder
                .decode(&self.bytes[val_start..(val_start + length)]);
            let new_encoder = Encoding::for_label(encoding_name.as_bytes());
            match new_encoder {
                Some(e) => {
                    self.encoder = e;
                    self.reader.encoder = e
                }
                None => {
                    return Err(ParserError::UnknownEncoding(
                        self.line,
                        encoding_name.to_string(),
                    ))
                }
            }

            let (c, _) = self.peek()?;
            if !is_xml_whitespace(c) && c != '?' {
                return Err(ParserError::Other(
                    "XML declaration: expected whitespace between attributes".to_owned(),
                ));
            }
            self.consume_whitespace();
        }

        // standalone (optional)
        let sstart = self.offset;
        if self.skip_if_matched("standalone") {
            self.consume_whitespace();
            let c = self.next()?;
            if c != '=' {
                return Err(ParserError::Other(
                    "XML declaration: unexpected character (expected '=')".to_string(),
                ));
            }
            let standalone = VtdRecord {
                kind: TokenType::DecAttrName,
                depth: 0,
                offset: sstart as u64,
                length: 10,
                prefix: None,
                source: None,
            };
            self.records.push(standalone);
            self.consume_whitespace();
            let quote = self.next()?;
            let ver_start = self.offset;
            if quote != '\'' && quote != '"' {
                return Err(ParserError::Other(
                    "XML declaration: unexpected character (expected opening quote)".to_string(),
                ));
            }
            if self.skip_if_matched("yes") {
                let version_attr = VtdRecord {
                    kind: TokenType::DecAttrVal,
                    depth: 0,
                    offset: ver_start as u64,
                    length: 3,
                    prefix: None,
                    source: None,
                };
                self.records.push(version_attr);
            } else if self.skip_if_matched("no") {
                let version_attr = VtdRecord {
                    kind: TokenType::DecAttrVal,
                    depth: 0,
                    offset: ver_start as u64,
                    length: 2,
                    prefix: None,
                    source: None,
                };
                self.records.push(version_attr);
            } else {
                return Err(ParserError::Other(
                    "XML declaration: when present, standalone must be 'yes' or 'no'".to_string(),
                ));
            }
            if self.next()? != quote {
                return Err(ParserError::Other(
                    "XML declaration: invalid closing quote".to_string(),
                ));
            }
            self.consume_whitespace();
        }

        if !self.skip_if_matched("?>") {
            return Err(ParserError::Other(
                "XML declaration not terminated properly".to_string(),
            ));
        }
        self.consume_whitespace();
        if self.next()? != '<' {
            return Err(ParserError::Other("Invalid character".to_string()));
        }
        Ok(ParseState::LtSeen)
    }

    /// Process an external entity, which begins with an optional text declaration
    fn process_external_entity_doc(&mut self) -> Result<ParseState, ParserError> {
        let (c, w) = self.peek()?;
        if c == '<' {
            self.advance(c, w);
            // starting with an text declaration ('<?xml ...')
            if self.skip_if_matched("?xml") {
                let (d, _) = self.peek()?;
                if is_xml_whitespace(d) {
                    self.consume_whitespace();
                    return Ok(ParseState::TextDecl);
                }
                // invalid '<?xml?>' (must at least have version attribute)
                if d == '?' {
                    return Err(ParserError::Other(
                        "Invalid text declaration in external entity".to_string(),
                    ));
                }
                // could have initial PI with a target like 'xmlfoo'
                // so fall through to non-declaration logic
            }
            self.depth = 0;
            return Ok(ParseState::LtSeen);
        }
        //whitespace followed by valid start character
        if is_xml_whitespace(c) {
            self.consume_whitespace();
        }
        self.depth = 0;
        Ok(ParseState::Text)
    }

    fn process_text_decl(&mut self) -> Result<ParseState, ParserError> {
        // required encoding
        // optional space
        let start = self.offset;
        // optional version
        if self.skip_if_matched("version") {
            self.consume_whitespace();
            let c = self.next()?;
            if c != '=' {
                return Err(ParserError::Other(
                    "Text declaration: unexpected character (expected '=')".to_string(),
                ));
            }
            let version = VtdRecord {
                kind: TokenType::DecAttrName,
                depth: 0,
                offset: start as u64,
                length: 7,
                prefix: None,
                source: None,
            };
            self.records.push(version);
            self.consume_whitespace();
            let quote = self.next()?;
            let ver_start = self.offset;
            if quote != '\'' && quote != '"' {
                return Err(ParserError::Other(
                    "Text declaration: unexpected character (expected opening quote)".to_string(),
                ));
            }
            if self.skip_if_matched("1.0") {
                let version_attr = VtdRecord {
                    kind: TokenType::DecAttrVal,
                    depth: 0,
                    offset: ver_start as u64,
                    length: 3,
                    prefix: None,
                    source: None,
                };
                self.records.push(version_attr);
            } else {
                return Err(ParserError::Other(
                    "Text declaration: unexpected version".to_string(),
                ));
            }
            if self.next()? != quote {
                return Err(ParserError::Other(
                    "Text declaration: invalid version closing quote".to_string(),
                ));
            }
            let (c, _) = self.peek()?;
            if !is_xml_whitespace(c) && c != '?' {
                return Err(ParserError::Other(
                    "Text declaration: expected whitespace between attributes".to_owned(),
                ));
            }
            self.consume_whitespace();
        }
        // encoding (required)
        let enc_start = self.offset;
        if !self.skip_if_matched("encoding") {
            return Err(ParserError::Other(
                "Encoding required in text declaration".to_string(),
            ));
        }
        self.consume_whitespace();
        let c = self.next()?;
        if c != '=' {
            return Err(ParserError::Other(
                "XML declaration: unexpected character (expected '=')".to_string(),
            ));
        }
        // save record
        let enc = VtdRecord {
            kind: TokenType::DecAttrName,
            depth: 0,
            offset: enc_start as u64,
            length: 10,
            prefix: None,
            source: None,
        };
        self.records.push(enc);
        let quote = self.next()?;
        let val_start = self.offset;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "Text declaration: unexpected character (expected opening quote)".to_string(),
            ));
        }
        // collect valid characters; break on quote
        let length = loop {
            let (c, w) = self.peek()?;
            // check that is valid
            if is_xml_whitespace(c) {
                return Err(ParserError::Other(
                    "Text declaration: whitespace not allowed in encoding name".to_string(),
                ));
            }
            if c == quote {
                break self.offset - val_start;
            }
            self.advance(c, w);
        };
        // write val record
        let tag = VtdRecord {
            kind: TokenType::DecAttrVal,
            depth: 0,
            offset: val_start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        if self.next()? != quote {
            return Err(ParserError::Other(
                "Text declaration: invalid closing quote".to_string(),
            ));
        }
        // switch the encoding to the specified value
        let (encoding_name, _, _) = self
            .encoder
            .decode(&self.bytes[val_start..(val_start + length)]);

        // if the encoding is UTF-16 or UTF-8 we've already auto-detected it
        // TODO: hmmm..validate this assertion, in case the file is incorrectly labelled
        if encoding_name != "UTF-16" && encoding_name != "UTF-8" {
            let new_encoder = Encoding::for_label(encoding_name.as_bytes());
            match new_encoder {
                Some(e) => {
                    self.encoder = e;
                    self.reader.encoder = e
                }
                None => {
                    return Err(ParserError::UnknownEncoding(
                        self.line,
                        encoding_name.to_string(),
                    ))
                }
            }
        }

        self.consume_whitespace();

        if !self.skip_if_matched("?>") {
            return Err(ParserError::Other(
                "Text declaration not terminated properly".to_string(),
            ));
        }
        self.consume_whitespace();
        self.depth = 0;
        Ok(ParseState::Text)
    }

    fn process_attr_name(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let mut prefix = None;
        // xmlns -- check for namespace decl
        let is_ns = self.skip_if_matched("xmlns");
        // if ws or '=' is default ns
        // else : is ns decl
        // collect name chars
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            // TODO: toggle off when namespaces disabled
            if c == ':' {
                prefix = Some((self.offset - start) as u32);
            }
            self.advance(c, w);
        };
        // validate decl if ns
        let kind = if is_ns {
            TokenType::AttrNs
        } else {
            TokenType::AttrName
        };

        let attr = VtdRecord {
            kind,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix,
            source: None,
        };
        // iterate over records backwards
        for rec in self.records.iter().rev() {
            match rec.kind {
                TokenType::AttrVal => continue,
                TokenType::AttrNs => (),
                TokenType::AttrName => (),
                _ => break,
            }
            self.check_attr_unique(rec, &attr)?;
        }
        self.records.push(attr);
        // consume optional whitespace
        self.consume_whitespace();
        if !self.skip_if('=') {
            return Err(ParserError::Other("Invalid char in attribute".to_string()));
        }
        self.consume_whitespace();
        Ok(ParseState::AttrVal)
    }

    fn check_attr_unique(&self, attr: &VtdRecord, existing: &VtdRecord) -> Result<(), ParserError> {
        // if len different, continue
        // read bytes, compare to current bytes (see endtag)
        // if same, return error
        if attr.length != existing.length {
            return Ok(());
        }
        let attr_start = attr.offset as usize;
        let attr_end = (attr.offset + u64::from(attr.length)) as usize;
        let attr_name = &self.bytes[attr_start..attr_end];

        let existing_start = existing.offset as usize;
        let existing_end = (existing.offset + u64::from(existing.length)) as usize;
        let existing_name = &self.bytes[existing_start..existing_end];
        if attr_name == existing_name {
            return Err(ParserError::DuplicateAttribute);
        }
        Ok(())
    }

    fn process_attr_val(&mut self) -> Result<ParseState, ParserError> {
        let quote = self.next()?;
        let start = self.offset;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "Attribute value: unexpected character (expected opening quote)".to_string(),
            ));
        }
        // collect valid characters; break on quote
        let length = loop {
            let (c, w) = self.peek()?;
            // TODO: if invalid error
            // watch out for <
            if c == '<' {
                return Err(ParserError::Other(
                    "Attribute value cannot contain < character".to_string(),
                ));
            }
            // check that is valid
            if c == quote {
                break self.offset - start;
            }
            self.advance(c, w);
            // watch out for entities
            if c == '&' {
                let v = self.check_entity(EntityContext::Attr)?;
                if v.contains('<') {
                    return Err(ParserError::Other(
                        "Attribute value cannot contain < character".to_string(),
                    ));
                }
            }
        };
        // need to validate ns values
        // write val record
        let tag = VtdRecord {
            kind: TokenType::AttrVal,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);

        // advance over close quote
        self.skip_if(quote);
        let (c, _) = self.peek()?;
        if is_namestart_char(c) {
            return Err(ParserError::Other(
                "Whitespace required between attribute/value pairs".to_owned(),
            ));
        }

        //after this is common with start_tag!
        self.complete_tag()
    }

    fn process_attr_def(&mut self) -> Result<(), ParserError> {
        let quote = self.next()?;
        let start = self.offset;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "Attribute default: unexpected character (expected opening quote)".to_string(),
            ));
        }
        // collect valid characters; break on quote
        let _length = loop {
            let (c, w) = self.peek()?;
            // TODO: if invalid error
            // watch out for <
            if c == '<' {
                return Err(ParserError::Other(
                    "Attribute default cannot contain < character".to_string(),
                ));
            }
            // check that is valid
            if c == quote {
                break self.offset - start;
            }
            self.advance(c, w);
            // watch out for entities
            if c == '&' {
                let v = self.check_entity(EntityContext::Attr)?;
                if v.contains('<') {
                    return Err(ParserError::Other(
                        "Attribute default cannot contain < character (even indirectly)"
                            .to_string(),
                    ));
                }
            }
        };

        // advance over close quote
        self.skip_if(quote);
        Ok(())
    }

    fn complete_tag(&mut self) -> Result<ParseState, ParserError> {
        self.consume_whitespace();
        let (mut c, mut w) = self.peek()?;

        // attribute
        if is_namestart_char(c) {
            return Ok(ParseState::AttrName);
        }
        // self-closing tag
        else if c == '/' {
            self.depth -= 1;
            self.advance(c, w);
            let (c1, w1) = self.peek()?;
            c = c1;
            w = w1;
            self.tag_stack.pop();
        }
        if c != '>' {
            self.advance(c, w);
            return Err(ParserError::Other(
                "Invalid character in start tag".to_string(),
            ));
        }
        self.advance(c, w);
        if self.depth == -1 {
            return Ok(ParseState::DocEnd);
        }
        self.find_next_state()
    }

    fn process_start_tag(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let mut prefix = None;

        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            // TODO: toggle off when namespaces disabled
            if c == ':' {
                prefix = Some((self.offset - start) as u32);
            }
            self.advance(c, w);
        };
        let tag = VtdRecord {
            kind: TokenType::StartTag,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix,
            source: None,
        };
        let ts = VtdRecord {
            kind: TokenType::StartTag,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix,
            source: None,
        };
        self.tag_stack.push(ts);
        self.records.push(tag);

        self.complete_tag()
    }

    fn process_end_tag(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        self.consume_whitespace();
        // get start tag for current depth
        if let Some(ts) = self.tag_stack.pop() {
            // compare to start tag
            let len = u64::from(ts.length);
            let tag_start = ts.offset as usize;
            let tag_end = (ts.offset + len) as usize;
            let tag_name = &self.bytes[tag_start..tag_end];
            // error if mismatch
            let end_tag_name = &self.bytes[start..(start + length)];
            if tag_name != end_tag_name {
                return Err(ParserError::MismatchedTag);
            }
        } else {
            return Err(ParserError::Other(
                "ENDTAG - Document not terminated properly".to_string(),
            ));
        }
        // ensure tag properly formed
        if !self.skip_if('>') {
            return Err(ParserError::Other(
                "End tag not properly formed".to_string(),
            ));
        }
        self.depth -= 1;
        if self.depth < 0 {
            return Ok(ParseState::DocEnd);
        }
        self.find_next_state()
    }

    fn process_end_doc(&mut self) -> Result<ParseState, ParserError> {
        self.consume_whitespace();
        let (c, _) = self.peek_opt();
        match c {
            None => Ok(ParseState::Complete),
            Some('<') => {
                if self.skip_if_matched("<?") {
                    Ok(ParseState::EndPi)
                } else if self.skip_if_matched("<!--") {
                    Ok(ParseState::EndComment)
                } else {
                    Err(ParserError::Other(
                        "Document not terminated properly".to_string(),
                    ))
                }
            }
            Some(_) => Err(ParserError::Other(
                "Document not terminated properly".to_string(),
            )),
        }
    }

    fn process_comment(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == '-' {
                let end = self.offset;
                self.advance(c, w);
                if self.skip_if('-') {
                    break end - start;
                } else {
                    continue;
                }
            }
            self.advance(c, w);
        };
        // found '--' in comment, expect '>'
        if !self.skip_if('>') {
            return Err(ParserError::Other("Comment improperly terminated; two hyphens ('--') is a prohibited sequence in comments".to_string()));
        }
        let tag = VtdRecord {
            kind: TokenType::Comment,
            // until root element is seen depth is -1, so clamp to 0
            depth: if self.depth < 0 { 0 } else { self.depth as u32 },
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        self.find_next_state()
    }

    fn process_end_comment(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == '-' {
                let end = self.offset;
                self.advance(c, w);
                if self.skip_if('-') {
                    break end - start;
                } else {
                    continue;
                }
            }
            self.advance(c, w);
        };
        // found '--' in comment, expect '>'
        if !self.skip_if('>') {
            return Err(ParserError::Other("Comment improperly terminated; two hyphens ('--') is a prohibited sequence in comments".to_string()));
        }
        let tag = VtdRecord {
            kind: TokenType::Comment,
            depth: if self.depth < 0 { 0 } else { self.depth as u32 },
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        Ok(ParseState::DocEnd)
    }

    fn process_cdata(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == ']' {
                let end = self.offset;
                self.advance(c, w);
                let checkpoint = self.offset;
                let (c, w) = self.peek()?;
                if c != ']' {
                    continue;
                }
                self.advance(c, w);
                let (c, _) = self.peek()?;
                if c != '>' {
                    self.reset_pos(checkpoint);
                    continue;
                }
                self.advance(c, w);
                break end - start;
            }
            self.advance(c, w);
        };
        let tag = VtdRecord {
            kind: TokenType::CDataVal,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        self.find_next_state()
    }

    fn process_pival(&mut self, state: Option<ParseState>) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == '?' {
                let end = self.offset;
                self.advance(c, w);
                if self.skip_if('>') {
                    break end - start;
                }
            }
            self.advance(c, w);
        };
        let tag = VtdRecord {
            kind: TokenType::PiVal,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        if let Some(s) = state {
            return Ok(s);
        }
        self.find_next_state()
    }

    fn process_end_pi(&mut self) -> Result<ParseState, ParserError> {
        let state = self.process_pi(Some(ParseState::DocEnd))?;
        if let ParseState::PiVal = state {
            let _ = self.process_pival(Some(ParseState::DocEnd))?;
        }
        Ok(ParseState::DocEnd)
    }

    fn process_pi(&mut self, state: Option<ParseState>) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        if length == 0 {
            return Err(ParserError::Other("Invalid PI tag".to_string()));
        }
        let tag = VtdRecord {
            kind: TokenType::PiName,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        let (tag_name, _, _) = self.encoder.decode(&self.bytes[start..(start + length)]);
        if tag_name.to_lowercase().eq("xml") {
            return Err(ParserError::Other(
                "PI tags cannot be named 'xml' (case-insensitive)".to_owned(),
            ));
        }
        //terminate right away
        let (c, _) = self.peek()?;
        if c == '?' {
            let val_start = self.offset;
            if !self.skip_if_matched("?>") {
                return Err(ParserError::Other(
                    "Invalid PI termination sequence".to_string(),
                ));
            } else {
                let tag = VtdRecord {
                    kind: TokenType::PiVal,
                    depth: self.depth as u32,
                    offset: val_start as u64,
                    length: 0_u32,
                    prefix: None,
                    source: None,
                };
                self.records.push(tag);
                if let Some(s) = state {
                    return Ok(s);
                }
                return self.find_next_state();
            }
        }
        self.consume_required_whitespace()?;
        Ok(ParseState::PiVal)
    }

    fn process_ignored_doctype(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let mut index = 1;
        loop {
            let c = self.next()?;
            if c == '>' {
                index -= 1;
            }
            if c == '<' {
                index += 1;
            }
            if index == 0 {
                break;
            }
        }
        let length = self.offset - start;
        let tag = VtdRecord {
            kind: TokenType::CDataVal,
            depth: 0_u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        self.consume_whitespace();
        if !self.skip_if('<') {
            return Err(ParserError::Other(
                "Invalid character after DOCTYPE declaration".to_string(),
            ));
        }
        Ok(ParseState::LtSeen)
    }
    /// When validation is disabled, we only check this is well-formed
    fn process_doctype(&mut self) -> Result<ParseState, ParserError> {
        if self.ignore_doctype {
            return self.process_ignored_doctype();
        }
        let start = self.offset;
        // space
        self.consume_required_whitespace()?;
        // name
        loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break;
            }
            self.advance(c, w);
        }
        let c = self.next()?;
        if c == '>' {
            // TODO: should we add a CData record like we do now?
            self.consume_whitespace();
            if !self.skip_if('<') {
                return Err(ParserError::Other(
                    "Invalid character after DOCTYPE declaration".to_string(),
                ));
            }
            return Ok(ParseState::LtSeen);
        }
        if c == '[' {
            return Ok(ParseState::DtdMarkup);
        }
        if !is_xml_whitespace(c) {
            return Err(ParserError::Other(
                "Invalid character after DOCTYPE name".to_string(),
            ));
        }
        self.consume_whitespace();

        let (c, w) = self.peek()?;
        // possibility one: SYSTEM id
        if c == 'S' {
            self.expect_match("SYSTEM")?;
            self.consume_required_whitespace()?;
            self.process_quoted_id()?;
            // TODO: resolve relative to base URI and parse contents
            self.consume_whitespace();
            let (c, w) = self.peek()?;
            if c == '>' {
                self.advance(c, w);
                self.consume_whitespace();
                if !self.skip_if('<') {
                    return Err(ParserError::Other(
                        "Invalid character after DOCTYPE declaration".to_string(),
                    ));
                }
                return Ok(ParseState::LtSeen);
            }
            if c == '[' {
                self.advance(c, w);
                return Ok(ParseState::DtdMarkup);
            }
            return Err(ParserError::Other(
                "Invalid character in DOCTYPE declaration".to_string(),
            ));
        }

        // possibility two: PUBLIC id
        if c == 'P' {
            self.expect_match("PUBLIC")?;
            self.consume_required_whitespace()?;
            self.process_public_id()?;
            self.consume_required_whitespace()?;
            self.process_quoted_id()?;
            self.consume_whitespace();
            let (c, w) = self.peek()?;
            if c == '>' {
                self.advance(c, w);
                self.consume_whitespace();
                if !self.skip_if('<') {
                    return Err(ParserError::Other(
                        "Invalid character after DOCTYPE declaration".to_string(),
                    ));
                }
                return Ok(ParseState::LtSeen);
            }
            if c == '[' {
                self.advance(c, w);
                return self.process_internal_subset();
            }
            return Err(ParserError::Other(
                "Invalid character in DOCTYPE declaration".to_string(),
            ));
        }

        // possibility three: internal subset
        if c == '[' {
            self.advance(c, w);
            return Ok(ParseState::DtdMarkup);
        }

        // possilibity four: end of DOCTYPE
        if c == '>' {
            // TODO: should we add a CData record like we do now?
            self.advance(c, w);
            self.consume_whitespace();
            if !self.skip_if('<') {
                return Err(ParserError::Other(
                    "Invalid character after DOCTYPE declaration".to_string(),
                ));
            }
            return Ok(ParseState::LtSeen);
        }

        let length = self.offset - start;
        let tag = VtdRecord {
            kind: TokenType::CDataVal,
            depth: 0_u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        self.records.push(tag);
        self.consume_whitespace();
        if !self.skip_if('<') {
            return Err(ParserError::Other(
                "Invalid character after DOCTYPE declaration".to_string(),
            ));
        }
        Ok(ParseState::LtSeen)
    }
    fn process_internal_subset(&mut self) -> Result<ParseState, ParserError> {
        loop {
            self.consume_whitespace();
            let c = self.next()?;
            if c == ']' {
                break;
            }
            if c == '%' {
                return self.expand_parameter_entity();
            }

            if c != '<' {
                return Err(ParserError::Other(
                    "Invalid character in internal subset declaration".to_string(),
                ));
            }
            if self.skip_if_matched("!ELEMENT") {
                return self.process_element_decl();
            }
            if self.skip_if_matched("!ENTITY") {
                return self.process_entity_decl();
            }
            if self.skip_if_matched("!ATTLIST") {
                return self.process_attlist_decl();
            }
            if self.skip_if_matched("!NOTATION") {
                return self.process_notation_decl();
            }
            if self.skip_if_matched("!--") {
                let _ = self.process_end_comment()?;
                return Ok(ParseState::DtdMarkup);
            }
            if self.skip_if_matched("?") {
                let state = self.process_pi(Some(ParseState::DtdMarkup))?;
                if let ParseState::PiVal = state {
                    let _ = self.process_pival(Some(ParseState::DtdMarkup))?;
                }
                return Ok(ParseState::DtdMarkup);
            }
        }

        self.consume_whitespace();
        if !self.skip_if('>') {
            return Err(ParserError::Other(
                "Invalid character after internal subset declaration".to_string(),
            ));
        }
        self.consume_whitespace();
        if !self.skip_if('<') {
            return Err(ParserError::Other(
                "Invalid character after DOCTYPE declaration".to_string(),
            ));
        }
        Ok(ParseState::LtSeen)
    }
    fn process_element_decl(&mut self) -> Result<ParseState, ParserError> {
        self.consume_required_whitespace()?;
        // capture name
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        let _entity_name = (start, length);
        self.consume_required_whitespace()?;
        // ANY
        if self.skip_if_matched("ANY") {
            self.consume_whitespace();
            let c = self.next()?;
            if c == '>' {
                return Ok(ParseState::DtdMarkup);
            }
            return Err(ParserError::Other(
                "Expected '>' to end element declaration".to_string(),
            ));
        }
        // EMPTY
        if self.skip_if_matched("EMPTY") {
            self.consume_whitespace();
            let c = self.next()?;
            if c == '>' {
                return Ok(ParseState::DtdMarkup);
            }
            return Err(ParserError::Other(
                "Expected '>' to end element declaration".to_string(),
            ));
        }
        let c = self.next()?;
        if c != '(' {
            return Err(ParserError::Other(
                "Expected content model in element declaration".to_string(),
            ));
        }
        self.consume_whitespace();
        if self.skip_if_matched("#PCDATA") {
            return self.process_mixed_content_decl();
        }
        self.process_element_content_decl(1)
    }
    fn process_element_content_decl(&mut self, depth: i32) -> Result<ParseState, ParserError> {
        //name OR '('
        let mut sep: Option<char> = None;

        loop {
            // either namestart OR (
            let start = self.offset;
            let c = self.next()?;
            if c == '(' {
                self.consume_whitespace();
                let _ = self.process_element_content_decl(depth + 1)?;
                self.consume_whitespace();
            } else if is_namestart_char(c) {
                // consume the name
                let _length = loop {
                    let (c, w) = self.peek()?;
                    if !is_name_char(c) {
                        break self.offset - start;
                    }
                    self.advance(c, w);
                };
                // optional operator
                let (c, w) = self.peek()?;
                if c == '*' || c == '?' || c == '+' {
                    self.advance(c, w);
                }
                self.consume_whitespace();
            } else {
                return Err(ParserError::Other(
                    "Invalid character in element content declaration".to_owned(),
                ));
            }
            let c = self.next()?;
            if c == ')' {
                break;
            }
            if c == ',' && sep.is_none() {
                sep = Some(',');
                self.consume_whitespace();
                continue;
            }
            if c == '|' && sep.is_none() {
                sep = Some('|');
                self.consume_whitespace();
                continue;
            }
            if c == ',' && sep == Some(',') {
                self.consume_whitespace();
                continue;
            }
            if c == '|' && sep == Some('|') {
                self.consume_whitespace();
                continue;
            }
            if c == '|' && sep == Some(',') {
                return Err(ParserError::Other(
                    "Cannot mix connectors in element content sequence".to_owned(),
                ));
            }
            if c == ',' && sep == Some('|') {
                return Err(ParserError::Other(
                    "Cannot mix connectors in element content sequence".to_owned(),
                ));
            }
        }
        let (c, w) = self.peek()?;
        if c == '*' || c == '?' || c == '+' {
            self.advance(c, w);
        }
        if depth == 1 {
            self.consume_whitespace();
            self.expect_match(">")?;
        }
        Ok(ParseState::DtdMarkup)
    }
    fn process_mixed_content_decl(&mut self) -> Result<ParseState, ParserError> {
        //optional space
        self.consume_whitespace();
        let c = self.next()?;
        if c == ')' {
            let _ = self.skip_if('*');
            self.consume_whitespace();
            self.expect_match(">")?;
            return Ok(ParseState::DtdMarkup);
        }
        if c != '|' {
            return Err(ParserError::Other(
                "Invalid character in mixed content declaration".to_owned(),
            ));
        }
        loop {
            self.consume_whitespace();
            let start = self.offset;
            let _length = loop {
                let (c, w) = self.peek()?;
                if !is_name_char(c) {
                    break self.offset - start;
                }
                self.advance(c, w);
            };
            self.consume_whitespace();
            let c = self.next()?;
            if c == ')' {
                break;
            }
            if c != '|' {
                return Err(ParserError::Other(
                    "Invalid character in mixed content declaration".to_owned(),
                ));
            }
        }
        self.expect_match("*")?;
        self.consume_whitespace();
        self.expect_match(">")?;
        Ok(ParseState::DtdMarkup)
    }
    fn process_entity_decl(&mut self) -> Result<ParseState, ParserError> {
        self.consume_required_whitespace()?;
        let (c, w) = self.peek()?;
        let mut is_pe = false;
        if c == '%' {
            self.advance(c, w);
            self.consume_required_whitespace()?;
            is_pe = true;
        }
        // capture name
        let (start, length) = self.require_name()?;
        let entity_name = self.read_as_string(start, length);
        self.consume_required_whitespace()?;
        let (c, _) = self.peek()?;
        if c == 'S' {
            // SYSTEM "literal"
            self.expect_match("SYSTEM")?;
            self.consume_required_whitespace()?;
            let system_id = self.process_system_id()?;
            let (c, _) = self.peek()?;
            if is_xml_whitespace(c) {
                self.consume_whitespace();
                let (c, _) = self.peek()?;
                if c == 'N' && !is_pe {
                    self.expect_match("NDATA")?;
                    self.consume_required_whitespace()?;
                    let (_, _) = self.require_name()?;
                    self.consume_whitespace();
                }
            }
            if self.next()? != '>' {
                return Err(ParserError::Other(
                    "expected '>' (end of entity definition)".to_string(),
                ));
            }
            // TODO: use the system ID as relative URL
            let r = self.resolve_system_id(system_id);
            let val = match r {
                Ok(external_uri) => self.process_external_entity(external_uri)?,
                // if validating, I believe need to propagate this error
                // spec says we should notify application we saw external entity?
                Err(ParserError::UnresolvedExternal(_, _)) => return Ok(ParseState::DtdMarkup),
                Err(x) => return Err(x),
            };

            // val probably needs to be an enum
            // expansion value <- parse on first lookup
            // fully expanded form <- essentially a cache
            // unresolved external <- for reporting purposes
            self.entity_decl.insert(entity_name, val);
            // resolve relative to base and parse the entity with subparser
            return Ok(ParseState::DtdMarkup);
        }
        if c == 'P' {
            // PUBLIC "id" "literal"
            self.expect_match("PUBLIC")?;
            self.consume_required_whitespace()?;
            self.process_public_id()?;
            self.consume_required_whitespace()?;
            self.process_quoted_id()?;
            self.consume_whitespace();
            let c = self.next()?;
            if c != '>' {
                return Err(ParserError::Other(
                    "expected '>' (end of entity definition)".to_string(),
                ));
            }
            return Ok(ParseState::DtdMarkup);
        }
        // entity value
        let val = self.process_entity_value()?;
        self.entity_decl.insert(entity_name, val);
        self.consume_whitespace();
        let c = self.next()?;
        if c != '>' {
            return Err(ParserError::Other(
                "expected '>' (end of entity definition)".to_string(),
            ));
        }
        Ok(ParseState::DtdMarkup)
    }

    // construct and return the replacement text
    fn process_entity_value(&mut self) -> Result<String, ParserError> {
        let quote = self.next()?;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "expected identifier enclosed in quotes".to_string(),
            ));
        }
        let mut start = self.offset;
        let mut prefix = String::new();
        let length = loop {
            let (c, w) = self.peek()?;
            if c == quote {
                //end of id
                let end = self.offset;
                self.advance(c, w);
                break end - start;
            }
            // expand general entity
            if c == '&' {
                let end = self.offset;
                let val = self.read_as_string(start, end - start);
                prefix.push_str(&val);
                self.advance(c, w);
                let v = self.check_entity(EntityContext::Entity)?;
                //println!("ENTITY {}", v);
                prefix.push_str(&v);
                start = self.offset;
                continue;
            }
            self.advance(c, w);
            // check parsed entity
            if c == '%' {
                return Err(ParserError::Other(
                    "Parsed entity cannot occur in entity value".to_owned(),
                ));
            }
        };
        let val = self.read_as_string(start, length);
        prefix.push_str(&val);
        Ok(prefix)
    }

    // if not validating we can just consume
    // instead of expanding the entity
    fn expand_parameter_entity(&mut self) -> Result<ParseState, ParserError> {
        let start = self.offset;
        let (length, width) = loop {
            let (c, w) = self.peek()?;
            if c == ';' {
                break (self.offset - start, w);
            }
            if !is_name_char(c) {
                return Err(ParserError::CharacterNotAllowed(
                    self.line,
                    c,
                    " in entity name".to_owned(),
                ));
            }
            self.advance(c, w);
        };
        self.advance(';', width);

        let entity_name = self.read_as_string(start, length);
        let decl = self.entity_decl.get(&entity_name);
        if let Some(val) = decl {
            let mut p = Parser::from_source(val.as_bytes(), UTF_8, ParseState::DtdMarkup);
            let r = parse_doc_loop(&mut p);
            match r {
                Err(ParserError::EOF) => Ok(p.state),
                Err(x) => Err(x),
                Ok(_) => Ok(ParseState::DtdMarkup),
            }
        } else {
            Err(ParserError::UnknownEntity(self.line, entity_name))
        }
    }
    fn process_quoted_id(&mut self) -> Result<String, ParserError> {
        let quote = self.next()?;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "expected identifier enclosed in quotes".to_string(),
            ));
        }
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == quote {
                let end = self.offset;
                //end of id
                self.advance(c, w);
                break end - start;
            }
            self.advance(c, w);
        };
        let val = self.read_as_string(start, length);
        Ok(val)
    }

    // the system ID is meant to be converted to an URI
    // per 4.2.2 External Entities it cannot contain a fragment("#")
    fn process_system_id(&mut self) -> Result<String, ParserError> {
        let quote = self.next()?;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "expected identifier enclosed in quotes".to_string(),
            ));
        }
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == '#' {
                return Err(ParserError::CharacterNotAllowed(
                    self.line,
                    c,
                    "in SYSTEM identifier".to_owned(),
                ));
            }
            if c == quote {
                //end of id
                let end = self.offset;
                self.advance(c, w);
                break end - start;
            }
            self.advance(c, w);
        };
        let val = self.read_as_string(start, length);
        Ok(val)
    }
    fn process_public_id(&mut self) -> Result<(), ParserError> {
        let quote = self.next()?;
        if quote != '\'' && quote != '"' {
            return Err(ParserError::Other(
                "expected identifier enclosed in quotes".to_string(),
            ));
        }
        loop {
            let (c, w) = self.peek()?;
            if c == quote {
                //end of id
                self.advance(c, w);
                break;
            }
            if !is_pubid_literal_char(c) {
                return Err(ParserError::Other(
                    "Invalid character found in PUBLIC id".to_string(),
                ));
            }
            self.advance(c, w);
        }
        Ok(())
    }

    fn process_attlist_decl(&mut self) -> Result<ParseState, ParserError> {
        self.consume_required_whitespace()?;
        // capture name
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        if length == 0 {
            return Err(ParserError::Other("ATTLIST name is required".to_owned()));
        }
        let _entity_name = (start, length);
        loop {
            self.consume_whitespace();
            let (c, w) = self.peek()?;
            if c == '>' {
                self.advance(c, w);
                return Ok(ParseState::DtdMarkup);
            }
            if !is_namestart_char(c) {
                return Err(ParserError::Other(
                    "Invalid character in ATTLIST definition".to_owned(),
                ));
            }
            self.process_attdef_decl()?;
        }
    }
    fn process_attdef_decl(&mut self) -> Result<ParseState, ParserError> {
        // capture name
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        let _attr_name = (start, length);
        self.consume_required_whitespace()?;
        // type is TOKEN or (a | b | c)
        let (c, w) = self.peek()?;
        match c {
            'I' => {
                if self.skip_if_matched("IDREF") {
                    let _ = self.skip_if_matched("S");
                } else {
                    self.expect_match("ID")?;
                }
                self.consume_required_whitespace()?;
            }
            'N' => {
                if self.skip_if_matched("NMTOKEN") {
                    let _ = self.skip_if_matched("S");
                    self.consume_required_whitespace()?;
                } else {
                    self.expect_match("NOTATION")?;
                    self.consume_required_whitespace()?;
                    self.expect_match("(")?;
                    self.consume_whitespace();
                    let c = self.next()?;
                    if !is_namestart_char(c) {
                        return Err(ParserError::Other(
                            "Invalid NAME in attribute definition".to_owned(),
                        ));
                    }
                    loop {
                        let c = self.next()?;
                        // nmtoken
                        if is_name_char(c) {
                            continue;
                        }
                        // end group
                        if c == ')' {
                            self.consume_required_whitespace()?;
                            break;
                        }
                        if c == '|' {
                            self.consume_whitespace();
                            let c = self.next()?;
                            if !is_namestart_char(c) {
                                return Err(ParserError::Other(
                                    "Invalid NAME in attribute definition".to_owned(),
                                ));
                            }
                            continue;
                        }
                        if is_xml_whitespace(c) {
                            self.consume_whitespace();
                            continue;
                        }
                        return Err(ParserError::CharacterNotAllowed(
                            self.line,
                            c,
                            "in attribute definition".to_owned(),
                        ));
                    }
                }
            }
            'E' => {
                if self.skip_if_matched("ENTITY") || self.skip_if_matched("ENTITIES") {
                    self.consume_required_whitespace()?;
                } else {
                    return Err(ParserError::Other(
                        "Unknown attribute type in definition".to_owned(),
                    ));
                }
            }
            'C' => {
                self.expect_match("CDATA")?;
                self.consume_required_whitespace()?;
            }
            '(' => {
                self.advance(c, w);
                self.consume_whitespace();
                let c = self.next()?;
                if !is_name_char(c) {
                    return Err(ParserError::Other(
                        "Invalid NMTOKEN in attribute definition".to_owned(),
                    ));
                }
                loop {
                    let c = self.next()?;
                    // nmtoken
                    if is_name_char(c) {
                        continue;
                    }
                    // end group
                    if c == ')' {
                        self.consume_required_whitespace()?;
                        break;
                    }
                    if c == '|' {
                        self.consume_whitespace();
                        let c = self.next()?;
                        if !is_name_char(c) {
                            return Err(ParserError::Other(
                                "Invalid NMTOKEN in attribute definition".to_owned(),
                            ));
                        }
                        continue;
                    }
                    if is_xml_whitespace(c) {
                        self.consume_whitespace();
                        continue;
                    }
                    return Err(ParserError::CharacterNotAllowed(
                        self.line,
                        c,
                        "in attribute definition".to_owned(),
                    ));
                }
            }

            _ => {
                return Err(ParserError::Other(
                    "Unknown attribute type in definition".to_owned(),
                ));
            }
        }
        // now handle default
        // #REQUIRED / #IMPLIED / #FIXED "val"
        // "val"
        let (c, _) = self.peek()?;
        match c {
            '\'' => {
                self.process_attr_def()?;
                Ok(ParseState::LtSeen)
            }
            '"' => {
                self.process_attr_def()?;
                Ok(ParseState::LtSeen)
            }
            '#' => {
                if self.skip_if_matched("#REQUIRED") {
                    return Ok(ParseState::LtSeen);
                }
                if self.skip_if_matched("#IMPLIED") {
                    return Ok(ParseState::LtSeen);
                }
                if self.skip_if_matched("#FIXED") {
                    self.consume_required_whitespace()?;
                    self.process_attr_def()?;
                    return Ok(ParseState::LtSeen);
                }
                Err(ParserError::Other(
                    "Invalid attribute default in definition".to_owned(),
                ))
            }
            _ => Err(ParserError::Other(
                "Invalid attribute default in definition".to_owned(),
            )),
        }
    }
    fn process_notation_decl(&mut self) -> Result<ParseState, ParserError> {
        self.consume_required_whitespace()?;
        // capture name
        let start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if !is_name_char(c) {
                break self.offset - start;
            }
            self.advance(c, w);
        };
        let _attr_name = (start, length);
        self.consume_required_whitespace()?;

        let (c, _) = self.peek()?;
        if c == 'S' {
            // SYSTEM "literal"
            self.expect_match("SYSTEM")?;
            self.consume_required_whitespace()?;
            self.process_system_id()?;
            self.consume_whitespace();
            let c = self.next()?;
            if c != '>' {
                return Err(ParserError::Other(
                    "expected '>' (end of notation definition)".to_owned(),
                ));
            }
            return Ok(ParseState::DtdMarkup);
        }
        if c == 'P' {
            // PUBLIC "id" ("literal")?
            self.expect_match("PUBLIC")?;
            self.consume_required_whitespace()?;
            self.process_public_id()?;
            // if there's whitespace there could be an optional literal
            let (c, _) = self.peek()?;
            if is_xml_whitespace(c) {
                self.consume_whitespace();
                let (c, _) = self.peek()?;
                if c == '"' || c == '\'' {
                    self.process_quoted_id()?;
                    self.consume_whitespace();
                }
            }
            let c = self.next()?;
            if c != '>' {
                return Err(ParserError::Other(
                    "expected '>' (end of notation definition)".to_owned(),
                ));
            }
            return Ok(ParseState::DtdMarkup);
        }
        Err(ParserError::Other(
            "expected SYSTEM or PUBLIC notation".to_owned(),
        ))
    }

    fn process_ex_seen(&mut self) -> Result<ParseState, ParserError> {
        if self.skip_if_matched("DOCTYPE") {
            return self.process_doctype();
        } else if self.skip_if_matched("[CDATA[") {
            if self.depth < 0 {
                return Err(ParserError::Other("CDATA not allowed here".to_string()));
            }
            return Ok(ParseState::CData);
        } else if self.skip_if_matched("--") {
            return Ok(ParseState::Comment);
        }
        Err(ParserError::Other(
            "Unrecognized sequence after <!".to_string(),
        ))
    }

    fn process_text(&mut self) -> Result<ParseState, ParserError> {
        if self.depth < 0 {
            return Err(ParserError::Other(
                "Character data outside elements".to_string(),
            ));
        }
        let mut start = self.offset;
        let length = loop {
            let (c, w) = self.peek()?;
            if c == '<' {
                break self.offset - start;
            }
            if c == ']' {
                self.advance(c, w);
                let checkpoint = self.offset;
                let (c, w) = self.peek()?;
                if c != ']' {
                    continue;
                }
                self.advance(c, w);
                let (c, _) = self.peek()?;
                if c != '>' {
                    self.reset_pos(checkpoint);
                    continue;
                }
                return Err(ParserError::Other(
                    "invalid CDATA section close found in text".to_string(),
                ));
            }
            if c == '&' {
                // end the existing text record
                let end = self.offset;
                // get the replacement text
                self.advance(c, w);
                //println!("about to expand at {}", self.offset);
                let replacement = self.check_entity(EntityContext::Content)?;
                // parse the entity replacement
                let mut entity_parser =
                    Parser::from_source(replacement.as_bytes(), UTF_8, ParseState::Text);
                entity_parser.depth = self.depth;
                entity_parser.entity_decl = self.entity_decl.clone();
                let r = parse_doc_loop(&mut entity_parser);
                match r {
                    // this is expected
                    Err(ParserError::EOF) => (),
                    // any other error - is an error!
                    Err(x) => return Err(x),
                    // this is fine
                    Ok(_) => (),
                };
                // TODO: check that p.depth == self.depth
                // this helps validate entity was well formed

                // -- just text, do nothing (rely on cursor expansion)
                if entity_parser.records.is_empty() {
                    continue;
                }
                // calc existing text record
                let prefix_len = end - start;
                if prefix_len > 0 {
                    let text = VtdRecord {
                        kind: TokenType::CharData,
                        depth: self.depth as u32,
                        offset: start as u64,
                        length: prefix_len as u32,
                        prefix: None,
                        source: None,
                    };
                    self.records.push(text);
                }
                println!("state {:?}", entity_parser.state);
                println!("TXT expansion result {:?}", entity_parser.records);
                for rec in entity_parser.records {
                    let e = VtdRecord {
                        kind: rec.kind,
                        depth: rec.depth,
                        offset: rec.offset,
                        length: rec.length,
                        prefix: rec.prefix,
                        source: Some(entity_parser.bytes.clone()),
                    };
                    self.records.push(e);
                }
                start = self.offset;
                continue;
            }
            self.advance(c, w);
        };
        let text = VtdRecord {
            kind: TokenType::CharData,
            depth: self.depth as u32,
            offset: start as u64,
            length: length as u32,
            prefix: None,
            source: None,
        };
        if text.length > 0 {
            self.records.push(text);
        }
        self.skip_if('<');
        Ok(ParseState::LtSeen)
    }

    // actual entity substition happens with Cursor
    fn check_entity(&mut self, context: EntityContext) -> Result<String, ParserError> {
        //println!("check_entity at {}, {:?}", self.offset, context);
        // recognize predefined entities
        // (amp quot apos lt gt)
        if self.skip_if_matched("lt;") {
            return Ok("&#60;".to_owned());
        }
        if self.skip_if_matched("gt;") {
            return Ok(">".to_owned());
        }
        if self.skip_if_matched("amp;") {
            return Ok("&#38;".to_owned());
        }
        if self.skip_if_matched("quot;") {
            return Ok("\"".to_owned());
        }
        if self.skip_if_matched("apos;") {
            return Ok("'".to_owned());
        }
        // recognize numeric character entities
        // (decimal or hex)
        let (c, w) = self.peek()?;
        if c == '#' {
            self.advance(c, w);
            let start = self.offset;
            let (c, w) = self.peek()?;
            match c {
                'x' => {
                    //hex entity
                    //let val = self.read_as_string(start, w);
                    //println!("found {} at {}", val, start);
                    self.advance(c, w);
                    let start = self.offset;
                    let (length, width) = loop {
                        let (c, w) = self.peek()?;
                        match c {
                            'a'..='f' => {
                                self.advance(c, w);
                                continue;
                            }
                            'A'..='F' => {
                                self.advance(c, w);
                                continue;
                            }
                            '0'..='9' => {
                                self.advance(c, w);
                                continue;
                            }
                            ';' => break (self.offset - start, w),
                            _ => {
                                return Err(ParserError::InvalidHexEntity);
                            }
                        }
                    };
                    self.advance(';', width);
                    let val = self.read_as_string(start, length);
                    //println!("found HEX {} at {}", val, start);
                    let num = val.trim_end_matches(';');
                    let nval = u32::from_str_radix(num, 16).unwrap();
                    let output = std::char::from_u32(nval).unwrap();
                    if !is_valid_char(output) {
                        return Err(ParserError::InvalidCharacter);
                    }
                    return Ok(output.to_string());
                }
                // decimal entity
                '0'..='9' => {
                    let (length, width) = loop {
                        let (c, w) = self.peek()?;
                        match c {
                            '0'..='9' => {
                                self.advance(c, w);
                                continue;
                            }
                            ';' => break (self.offset - start, w),
                            _ => {
                                return Err(ParserError::InvalidNumericEntity);
                            }
                        }
                    };
                    self.advance(';', width);
                    let val = self.read_as_string(start, length);
                    let num = val.trim_end_matches(';').parse::<u32>().unwrap();
                    let output = std::char::from_u32(num).unwrap();
                    if !is_valid_char(output) {
                        return Err(ParserError::InvalidCharacter);
                    }
                    return Ok(output.to_string());
                }
                _ => {
                    return Err(ParserError::InvalidNumericEntity);
                }
            }
        }
        // name_start_char means other named entity
        if is_namestart_char(c) {
            let start = self.offset;
            self.advance(c, w);
            let (length, width) = loop {
                let (c, w) = self.peek()?;
                if c == ';' {
                    break (self.offset - start, w);
                }
                if !is_name_char(c) {
                    return Err(ParserError::CharacterNotAllowed(
                        self.line,
                        c,
                        " in entity name".to_owned(),
                    ));
                }
                self.advance(c, w);
            };
            self.advance(';', width);

            let entity_name = self.read_as_string(start, length);
            let decl = self.entity_decl.get(&entity_name);
            println!(
                "ENTITY {} at {} len {} from {}",
                entity_name, start, length, &self.bytes[start]
            );
            match decl {
                None => return Err(ParserError::UnknownEntity(self.line, entity_name)),
                Some(val) => {
                    // if context is a general entity value, we bypass
                    // which means return &name; as the replacement text
                    if context == EntityContext::Entity {
                        return Ok(format!("&{};", entity_name));
                    }
                    return Ok(val.to_string());
                }
            }
        }
        Err(ParserError::Other("Invalid entity found".to_string()))
    }

    fn find_next_state(&mut self) -> Result<ParseState, ParserError> {
        let ws_start = self.offset;
        self.consume_whitespace();
        if self.skip_if('<') {
            //  < (add whitespace node, then LTSEEN (peek /, treat previous whitespace as content?, ENDTAG)
            return Ok(ParseState::LtSeen);
        }
        //  & ENTITY
        if self.skip_if_matched("]]>") {
            return Err(ParserError::Other(
                "invalid CDATA section close found in text".to_string(),
            ));
        }
        self.reset_pos(ws_start);
        Ok(ParseState::Text)
    }
}

//pub fn build_index(Vec<VtdRecord> -> Vec<LocationCache> {
// filter to just StartTags
// first one (depth 0) is root
// each LC record = (VTD index, index of first child) tuple
// array of LC records for each level of depth
// after depth X just do a straight VTD scan
//}

// autodetect the encoding and BOM
pub fn guess_encoding(bytes: &[u8]) -> (&'static Encoding, bool) {
    // skipping encodings that aren't covered by the encoding-rs crate
    // notably UCS-4 and EBCDIC; those are both in XML standard
    match bytes {
        [0xFE, 0xFF, _, _] => (UTF_16BE, true),
        [0xFF, 0xFE, _, _] => (UTF_16LE, true),
        [0xEF, 0xBB, 0xBF, _] => (UTF_8, true),

        b"\x3C\x3F\x78\x6D" => (UTF_8, false),
        b"\x3C\x00\x3F\x00" => (UTF_16LE, false),
        b"\x00\x3C\x00\x3F" => (UTF_16BE, false),
        _ => (UTF_8, false),
    }
}

fn parse_doc_loop(parser: &mut Parser) -> Result<(), ParserError> {
    loop {
        parser.state = match parser.state {
            ParseState::DocStart => parser.process_start_doc()?,
            ParseState::ExternalStart => parser.process_external_entity_doc()?,
            ParseState::DocEnd => parser.process_end_doc()?,
            ParseState::DecAttrName => parser.process_decl_attr()?,
            ParseState::TextDecl => parser.process_text_decl()?,
            ParseState::StartTag => parser.process_start_tag()?,
            ParseState::EndTag => parser.process_end_tag()?,
            ParseState::AttrName => parser.process_attr_name()?,
            ParseState::AttrVal => parser.process_attr_val()?,
            ParseState::Comment => parser.process_comment()?,
            ParseState::CData => parser.process_cdata()?,
            ParseState::PiTag => parser.process_pi(None)?,
            ParseState::PiVal => parser.process_pival(None)?,
            ParseState::EndComment => parser.process_end_comment()?,
            ParseState::EndPi => parser.process_end_pi()?,
            ParseState::DtdMarkup => parser.process_internal_subset()?,
            ParseState::Text => parser.process_text()?,
            ParseState::LtSeen => {
                //let curr = parser.offset;
                let (c, w) = parser.peek()?;
                if is_namestart_char(c) {
                    parser.depth += 1;
                    //parser.offset = curr;
                    ParseState::StartTag
                } else {
                    parser.advance(c, w);
                    match c {
                        '/' => ParseState::EndTag,
                        '?' => ParseState::PiTag,
                        '!' => parser.process_ex_seen()?,
                        _ => {
                            return Err(ParserError::Other("Invalid character after <".to_string()))
                        }
                    }
                }
            }
            ParseState::Complete => {
                break;
            }
        }
    }
    Ok(())
}

pub fn parse_doc_impl(
    bytes: Rc<[u8]>,
    base: Option<Box<PathBuf>>,
) -> Result<(Vec<VtdRecord>, &'static Encoding), ParserError> {
    // check first four bytes to guess encoding
    if bytes.len() < 4 {
        return Err(ParserError::Other("Not enough bytes".to_string()));
    }
    let (enc, bom) = guess_encoding(&bytes[0..4]);
    // otherwise default to UTF-8
    let mut parser = Parser::new(bytes, enc);
    parser.xml_base = base;
    // skip the BOM if one exists
    if bom {
        if enc == UTF_8 {
            parser.offset = 3;
            parser.reader.advance(3);
        } else {
            parser.offset = 2;
            parser.reader.advance(2);
        }
    }
    parse_doc_loop(&mut parser)?;
    Ok((parser.records, parser.encoder))
}

pub fn parse_ext_entity_impl(
    bytes: Rc<[u8]>,
    base: Option<Box<PathBuf>>,
) -> Result<(Vec<VtdRecord>, &'static Encoding), ParserError> {
    // check first four bytes to guess encoding
    // external entity can be less than 4 bytes
    let (enc, bom) = if bytes.len() < 4 {
        (UTF_8, false)
    } else {
        guess_encoding(&bytes[0..4])
    };
    // otherwise default to UTF-8
    let mut parser = Parser::new(bytes, enc);
    parser.xml_base = base;
    parser.state = ParseState::ExternalStart;
    // skip the BOM if one exists
    if bom {
        if enc == UTF_8 {
            parser.offset = 3;
        } else {
            parser.offset = 2;
        }
    }
    let r = parse_doc_loop(&mut parser);
    match r {
        Err(ParserError::EOF) => Ok((parser.records, parser.encoder)),
        Err(x) => Err(x),
        Ok(_) => Ok((parser.records, parser.encoder)),
    }
}

/// The tests here are mostly for the low-level helper functions
/// Most coverage is actually handled by the lib.rs tests
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parser_skip_if() {
        let s = "<foo />";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        assert_eq!('<', parser.next().unwrap());
        assert_eq!(1, parser.offset);
        // don't advance offset when we peek
        let (c, _) = parser.peek().unwrap();
        assert_eq!('f', c);
        assert_eq!(1, parser.offset);
        // expect offset to advance on match
        assert!(parser.skip_if('f'), "Failed to skip matching f");
        assert_eq!(2, parser.offset);
        // expect offset to not advance if no match
        assert_eq!(
            false,
            parser.skip_if('z'),
            "Incorrectly skipped non-matching z"
        );
        assert_eq!(2, parser.offset);
        // expect offset to advance on match
        assert!(parser.skip_if('o'), "Failed to skip matching o");
        assert_eq!(3, parser.offset);
    }

    #[test]
    fn parser_eat_ws() {
        let s = "  \n<foo />";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        parser.consume_whitespace();
        assert_eq!('<', parser.next().unwrap());
    }

    #[test]
    fn parser_skip_if_matched() {
        let s = "foobar";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        // expect offset to advance on match
        assert!(
            parser.skip_if_matched("foo"),
            "Failed to skip matching 'foo'"
        );
        assert_eq!(3, parser.offset);
        // expect offset to not advance if no match
        assert_eq!(
            false,
            parser.skip_if_matched("baz"),
            "Incorrectly skipped non-matching 'baz'"
        );
        assert_eq!(3, parser.offset);
    }

    #[test]
    fn parser_expect_match() -> Result<(), ParserError> {
        let s = "foobar";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        // expect offset to advance on match
        assert_eq!((), parser.expect_match("foo")?);
        assert_eq!(3, parser.offset);
        // expect offset to not advance if no match
        assert_eq!(
            "Line 1: expected \'baz\' at this location",
            parser.expect_match("baz").unwrap_err().to_string()
        );
        assert_eq!(
            false,
            parser.skip_if_matched("baz"),
            "Incorrectly skipped non-matching 'baz'"
        );
        assert_eq!(3, parser.offset);
        Ok(())
    }

    #[test]
    fn parser_expand_numeric_entity() {
        let s = "#60;";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        let val = parser.check_entity(EntityContext::Content).unwrap();
        assert_eq!("<", val);
    }

    #[test]
    fn parser_expand_hex_entity() {
        let s = "#x01f355;";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        let val = parser.check_entity(EntityContext::Content).unwrap();
        assert_eq!("\u{01f355}", val);
    }

    #[test]
    fn parser_expand_general_text_entity() {
        let s = "LEFT-&rights;<";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        parser.depth = 0;
        parser
            .entity_decl
            .insert("rights".to_string(), "RIGHTS".to_string());
        let val = parser.process_text().unwrap();
        // expect single text record
        assert_eq!(1, parser.records.len());
        assert_eq!(TokenType::CharData, parser.records[0].kind);
        assert_eq!(13, parser.records[0].length);
        assert_eq!(ParseState::LtSeen, val);
    }

    #[test]
    fn parser_expand_general_tag_entity() {
        let s = "prefix&rights;<";
        let mut parser = Parser::new(s.as_bytes().into(), UTF_8);
        parser.depth = 0;
        parser
            .entity_decl
            .insert("rights".to_string(), "<foo />".to_string());
        let val = parser.process_text().unwrap();
        println!("FINAL {:?}", parser.records);
        assert_eq!(2, parser.records.len());
        // "prefix" text
        assert_eq!(TokenType::CharData, parser.records[0].kind);
        assert_eq!(6, parser.records[0].length);
        // start tag (from expanded entity)
        assert_eq!(TokenType::StartTag, parser.records[1].kind);
        assert_eq!(3, parser.records[1].length);
        assert!(parser.records[1].source.is_some());
        assert_eq!(ParseState::LtSeen, val);
    }
}
